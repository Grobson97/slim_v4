import gdspy
from models.alignment_cross import AlignmentCross


def main():
    library = gdspy.GdsLibrary("Resonator Library", unit=1e-06, precision=1e-09)
    # Create Lekid cell to add module cells to
    main_cell = library.new_cell("Main")

    test_alignment = AlignmentCross(
        centre_x=0.0, centre_y=0.0, cross_size=(300.0, 5.0), box_size=(1000.0, 20.0)
    )

    test_alignment_cell = test_alignment.make_cell(layer=0, cell_name="Alignment 1")
    main_cell.add(test_alignment_cell)

    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
