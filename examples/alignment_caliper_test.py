import gdspy
from models.alignment_caliper import AlignmentCaliper


def main():
    library = gdspy.GdsLibrary("Resonator Library", unit=1e-06, precision=1e-09)
    # Create Lekid cell to add module cells to
    main_cell = library.new_cell("Main")

    test_caliper = AlignmentCaliper(
        notch_spacing=2.0,
        notch_height=8,
        notch_width=3,
        notch_count=5,
        off_set_per_notch=0.2,
        row_spacing=0.0,
        centre=(0.0, 0.0)
    )

    test_caliper_cell = test_caliper.make_cell(bottom_layer=0, top_layer=1, cell_name="Alignment 1")
    main_cell.add(test_caliper_cell)

    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
