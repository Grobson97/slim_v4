import gdspy
from models.dc_resistance_test import DCResistanceTest
import util.filter_bank_builder_tools as tools
import numpy as np


def main():
    library = gdspy.GdsLibrary("Resonator Library", unit=1e-06, precision=1e-09)
    # Create Lekid cell to add module cells to
    main_cell = library.new_cell("Main")

    dc_resistance_test = DCResistanceTest(
        microstrip_width=2.0,
        microstrip_length=2000.0,
        meander_width=100,
        origin=(0.0, 0.0),
    )

    dc_resistance_test_cell = dc_resistance_test.make_cell(
        microstrip_layer=0,
        cell_name="DC Resistance Test",
    )

    dc_resistance_test_cell_reference = gdspy.CellReference(
        dc_resistance_test_cell, (0.0, 0.0), rotation=0.0
    )

    main_cell.add(dc_resistance_test_cell_reference)

    print(dc_resistance_test.get_output_coordinate())
    print(dc_resistance_test.get_midpoint_coordinate())

    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
