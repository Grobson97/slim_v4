import gdspy
from models.lekid import Lekid


def main():
    library = gdspy.GdsLibrary("Resonator Library", unit=1e-06, precision=1e-09)
    # Create Lekid cell to add module cells to
    main_cell = library.new_cell("Main")

    test_lekid = Lekid(
        target_f0=2.5e9,
        signal_input_x=0.0,
        signal_input_y=0.0,
    )

    test_lekid_cell = test_lekid.make_cell(
        inductor_layer=0,
        idc_layer=1,
        readout_layer=2,
        ground_layer=3,
        dielectric_layer=4,
        nitride_step_down_layer=5,
        oxide_step_down_layer=6,
        e_beam_boundary_layer=7,
        readout_width=16.0,
        readout_gap=10.0,
        cell_name="Lekid #A123",
    )
    main_cell.add(test_lekid_cell)
    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
