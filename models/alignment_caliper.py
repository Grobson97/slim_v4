import gdspy
import numpy as np


class AlignmentCaliper:
    def __init__(
        self,
        notch_spacing: float,
        notch_width: float,
        notch_height: float,
        notch_count: int,
        off_set_per_notch: float,
        row_spacing=0.0,
        centre=(0, 0),
    ) -> None:
        """
        Creates a new of an alignment calliper, typically used for the checking alignments of different layers. Calliper
        is created horizontally.

        :param notch_spacing: Spacing between notches.
        :param notch_width: width of each notch (length in x).
        :param notch_height: Height of each notch (length in y).
        :param notch_count: Number of notches on each side of the centre line.
        :param off_set_per_notch: Shift of each notch away from the centre in aligning calliper.
        :param row_spacing: Vertical spacing between each layers callipers.
        :param centre: Coordinates for the centre of the cross.
        """

        self.centre = centre
        self.notch_spacing = notch_spacing
        self.notch_width = notch_width
        self.notch_height = notch_height
        self.notch_count = notch_count
        self.off_set_per_notch = off_set_per_notch
        self.row_spacing = row_spacing

    def make_cell(
        self,
        bottom_layer: int,
        top_layer: int,
        cell_name="Alignment Caliper",
    ) -> gdspy.Cell:
        """
        Returns the gdspy Cell for a given BongPad instance.

        :param layer: GDSII layer for the alignment mark.
        :param cell_name: Name to be given to the Cell.
        """

        caliper_cell = gdspy.Cell(cell_name)
        bottom_caliper_cell = gdspy.Cell(cell_name + "Bottom")
        top_caliper_cell = gdspy.Cell(cell_name + "Top")

        top_centre_line = gdspy.Rectangle(
            (self.centre[0] - self.notch_width / 2, self.centre[1]),
            (self.centre[0] + self.notch_width / 2, self.centre[1] + 2 * self.notch_height),
            layer=top_layer
        )

        bottom_centre_line = gdspy.Rectangle(
            (self.centre[0] - self.notch_width / 2, self.centre[1]),
            (self.centre[0] + self.notch_width / 2, self.centre[1] + 2 * self.notch_height),
            layer=bottom_layer
        )

        top_caliper_cell.add(top_centre_line)
        bottom_caliper_cell.add(bottom_centre_line)


        top_notch_cell = gdspy.Cell(cell_name + "Top Notch Old Chum")
        top_notch = gdspy.Rectangle(
            (0.0, 0.0),
            (self.notch_width, self.notch_height),
            layer=top_layer
        )
        top_notch_cell.add(top_notch)

        bottom_notch_cell = gdspy.Cell(cell_name + "Bottom Notch")
        bottom_notch = gdspy.Rectangle(
            (0.0, 0.0),
            (self.notch_width, self.notch_height),
            layer=bottom_layer
        )
        bottom_notch_cell.add(bottom_notch)

        offset = self.off_set_per_notch
        for n in range(self.notch_count):
            top_caliper_cell.add([
                gdspy.CellReference(top_notch_cell, origin=(
                    self.notch_width/2 + (n + 1) * self.notch_spacing + n * self.notch_width, 0.0)),
                gdspy.CellReference(top_notch_cell, origin=(
                    -self.notch_width / 2 - (n + 1) * self.notch_spacing - (n + 1) * self.notch_width, 0.0))
            ])

            bottom_caliper_cell.add([
                gdspy.CellReference(bottom_notch_cell, origin=(
                    self.notch_width/2 + (n + 1) * self.notch_spacing + n * self.notch_width - offset, 0.0)),
                gdspy.CellReference(bottom_notch_cell, origin=(
                    -self.notch_width / 2 - (n + 1) * self.notch_spacing - (n + 1) * self.notch_width + offset, 0.0))
            ])

            offset += self.off_set_per_notch

        caliper_cell.add(
            gdspy.CellReference(bottom_caliper_cell, x_reflection=True, origin=(0.0, -self.row_spacing / 2))
        )
        caliper_cell.add(
            gdspy.CellReference(top_caliper_cell, origin=(0.0, self.row_spacing / 2))
        )

        return caliper_cell
