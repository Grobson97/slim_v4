import gdspy
import numpy as np
import util.filter_bank_builder_tools as filter_bank_builder_tools


def make_cpw_to_microstrip(cell_name: str, antenna_layer: int, ground_layer: int):
    transition_cell = gdspy.Cell(cell_name + "cpw to u-strip")

    # using layer 100 as a construction layer

    cpw_feedline_path = gdspy.FlexPath(
        points=[(0.0, 0.0), (921.5, 0.0)],
        width=[2.0, 100.0],
        offset=0.0,
        corners="circular bend",
        bend_radius=30.0,
        layer=[antenna_layer, 100],
    )
    cpw_trench_path = gdspy.FlexPath(
        points=[(0.0, 0.0), (921.5, 0.0)],
        width=10.0,
        offset=0.0,
        corners="circular bend",
        bend_radius=30.0,
        layer=ground_layer,
    )

    # Create bridges in ground plane as the cpw to microstrip transition:
    bridge1 = gdspy.Rectangle(
        (50.0, 5),
        (54.0, -5),
        layer=ground_layer,
    )
    bridge2 = gdspy.Rectangle(
        (270, 5),
        (280, -5),
        layer=ground_layer,
    )
    bridge3 = gdspy.Rectangle(
        (423, 5),
        (449.5, -5),
        layer=ground_layer,
    )
    bridge4 = gdspy.Rectangle(
        (573.5, 5),
        (636, -5),
        layer=ground_layer,
    )
    bridge5 = gdspy.Rectangle(
        (703, 5),
        (796, -5),
        layer=ground_layer,
    )
    bridge6 = gdspy.Rectangle(
        (818, 5),
        (914, -5),
        layer=ground_layer,
    )
    bridges = [bridge1, bridge2, bridge3, bridge4, bridge5, bridge6]
    # Remove bridges from cpw_trench_path.
    cpw_trench_path = gdspy.boolean(cpw_trench_path, bridges, "not", layer=ground_layer)

    transition_cell.add([cpw_feedline_path, cpw_trench_path])

    return transition_cell


class OMT:
    def __init__(
        self,
        diameter: float,
        choke_width: float,
        dual_polarisation: True,
        output_bend_angle=15,
        centre_x=0.0,
        centre_y=0.0,
    ) -> None:
        """
        Creates a new instance of an orthomode transducer antenna. NB: All
        dimensions must be in micrometers.

        :param: diameter: Diameter of the OMT. The same diameter as the end of
        the feed horn.
        :param: choke_width: Distance between the inner and outer diameter of
        the choke.
        :param: dual_polarisation: Boolean, True means 2 pairs of antenna
        paddles. If false, only the 12 O'clock and 6 O'clock pair is built.
        :param output_bend_angle: Angle to which the probe outputs are bent away from the ordinate axes.
        :param: centre_x: x-coordinate of the centre of the OMT.
        :param: centre_y: x-coordinate of the centre of the OMT.
        """

        self.diameter = diameter
        self.choke_width = choke_width
        self.dual_polarisation = dual_polarisation
        self.output_bend_angle = output_bend_angle
        self.centre_x = centre_x
        self.centre_y = centre_y

    def make_cell(
        self,
        antenna_layer: int,
        ground_layer: int,
        dielectric_layer: int,
        back_etch_layer: int,
        cell_name="OMT",
        tolerance=0.01,
    ) -> gdspy.Cell:
        """
        Returns the gdspy Cell for a given OMT instance.

        :param: antenna_layer: GDSII layer for the antenna paddles.
        :param: ground_layer: GDSII layer for the ground plane.
        :param: dielectric_layer: GDSII layer for the dielectric.
        :param: back_etch_layer: GDSII layer for the etching of the bulk wafer.
        :param: cell_name: Name to be used to reference the cell.
        :param: tolerance: Approximate curvature resolution. The number of
        points is automatically calculated.
        """

        radius = self.diameter / 2
        centre_x = self.centre_x
        centre_y = self.centre_y

        omt_cell = gdspy.Cell(cell_name)

        # Make negative hole for ground layer:
        ground_hole = gdspy.Round(
            (centre_x, centre_y),
            radius=radius,
            tolerance=tolerance,
            layer=ground_layer,
        )

        # Make negative hole for dielectric layer:
        dielectric_hole = gdspy.Round(
            (centre_x, centre_y),
            radius=radius,
            tolerance=tolerance,
            layer=dielectric_layer,
        )

        # Make negative hole for back etch layer:
        back_etch_hole = gdspy.Round(
            (centre_x, centre_y),
            radius=radius + self.choke_width,
            tolerance=tolerance,
            layer=back_etch_layer,
        )

        # Make negative hole for back etch layer:
        reduced_dielectric_polygon = gdspy.Round(
            (centre_x, centre_y),
            radius=radius + self.choke_width + 100,
            tolerance=tolerance,
            layer=dielectric_layer,
        )

        probes = gdspy.Cell(cell_name + "Probes")

        probe_vertices = [
            (1.0, 0.0),
            (1.0, -5.0),
            (1.0, -5.0),
            (100.0, -155.0),
            (100.0, -505.0),
            (-100.0, -505.0),
            (-100.0, -155.0),
            (-1.0, -5.0),
            (-1.0, -5.0),
            (-1.0, 0.0),
            (1.0, 0.0),
        ]

        probe_cell = gdspy.Cell(cell_name + "Probe")
        probe_cell.add(gdspy.Polygon(probe_vertices, layer=antenna_layer))

        cpw_to_microstrip_cell = make_cpw_to_microstrip(
            cell_name=cell_name, antenna_layer=antenna_layer, ground_layer=ground_layer
        )

        antenna_cpw_connection_cell = gdspy.Cell(cell_name + "antenna cpw connection")
        output_bend_angle = self.output_bend_angle
        output_bend_angle_radians = (output_bend_angle / 360) * 2 * np.pi
        antenna_connection_path = gdspy.FlexPath(
            points=[
                (0.0, 0.0),
                (50.0, 0.0),
                (
                    50 + 50 * np.cos(output_bend_angle_radians),
                    50 * np.sin(output_bend_angle_radians),
                ),
            ],
            width=2.0,
            offset=0.0,
            corners="circular bend",
            bend_radius=30.0,
            layer=antenna_layer,
        )
        antenna_connection_trench = gdspy.FlexPath(
            points=[
                (-5, 0.0),
                (50.0, 0.0),
                (
                    50 + 50 * np.cos(output_bend_angle_radians),
                    50 * np.sin(output_bend_angle_radians),
                ),
            ],
            width=10.0,
            offset=0.0,
            corners="circular bend",
            bend_radius=30.0,
            layer=ground_layer,
        )

        antenna_cpw_connection_cell.add(
            [antenna_connection_path, antenna_connection_trench]
        )

        antenna_cpw_connection_cell.add(
            gdspy.CellReference(
                cpw_to_microstrip_cell,
                origin=(
                    50 + 50 * np.cos(output_bend_angle_radians),
                    50 * np.sin(output_bend_angle_radians),
                ),
                rotation=output_bend_angle,
            )
        )

        # Add cpw to microstrip transition and probes to probes cell.
        cpw_to_microstrip1 = gdspy.CellReference(
            antenna_cpw_connection_cell, origin=(radius, 0.0), rotation=0.0
        )
        cpw_to_microstrip2 = gdspy.CellReference(
            antenna_cpw_connection_cell, origin=(-radius, 0.0), x_reflection=True, rotation=180
        )
        probe1 = gdspy.CellReference(probe_cell, origin=(0.0, radius), rotation=0.0)
        probe2 = gdspy.CellReference(probe_cell, origin=(0.0, -radius), rotation=180.0)
        probes.add([probe1, probe2, cpw_to_microstrip1, cpw_to_microstrip2])

        # Make dielectric ring to remove dielectric from everywhere except under the paths.
        reduced_dielectric_polygon = gdspy.boolean(
            operand1=reduced_dielectric_polygon,
            operand2=dielectric_hole,
            operation="not",
            layer=99
        )

        # If to add other pair if dual polarisation is desired:
        if self.dual_polarisation == True:
            probe3 = gdspy.CellReference(
                probe_cell, origin=(-radius, 0.0), rotation=90.0
            )
            probe4 = gdspy.CellReference(
                probe_cell, origin=(radius, 0.0), rotation=270.0
            )
            cpw_to_microstrip3 = gdspy.CellReference(
                antenna_cpw_connection_cell, origin=(0.0, -radius), rotation=-90.0
            )
            cpw_to_microstrip4 = gdspy.CellReference(
                antenna_cpw_connection_cell,
                origin=(0.0, radius),
                rotation=90.0,
                x_reflection=True,
            )
            probes.add([probe3, probe4, cpw_to_microstrip3, cpw_to_microstrip4])

        omt_cell.add(ground_hole)
        omt_cell.add(dielectric_hole)
        omt_cell.add(reduced_dielectric_polygon)
        omt_cell.add(back_etch_hole)
        omt_cell.add(probes)

        return omt_cell

    def get_feedline_connections(self) -> list:
        """
        Function to return the x and y coordinates of the connection points to
        each antenna. The coordinates are returned in the format
        [(x1,y1), (x2,y2)...] to a maximum of four different connection points.
        For the non-rotated OMT the order is as follows:\n
        1: 12 O'clock\n
        2: 6  O'clock\n
        3: 3 O'clock\n
        4: 9 O'clock\n
        """

        radius = self.diameter / 2

        relative_connection = (
            radius + 50 + np.cos((self.output_bend_angle / 360) * 2 * np.pi) * (921.5 + 50),
            np.sin((self.output_bend_angle / 360) * 2 * np.pi) * (921.5 + 50)
        )

        connections = [
            filter_bank_builder_tools.rotate_coordinates(relative_connection[0], -relative_connection[1], np.pi / 2),
            filter_bank_builder_tools.rotate_coordinates(relative_connection[0], relative_connection[1], -np.pi / 2),
        ]

        if self.dual_polarisation == True:
            connections.append((relative_connection[0], relative_connection[1]))
            connections.append(filter_bank_builder_tools.rotate_coordinates(relative_connection[0], -relative_connection[1], np.pi))

        return connections
