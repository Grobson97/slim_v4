import gdspy
import numpy
import math


class Inductor:
    def __init__(
        self,
        track_width: float,
        bend_radius: float,
        signal_input_connection_x: float,
        signal_input_connection_y: float,
        tolerance=0.01,
    ):
        """
        Creates new instance of an inductor section.

        :param track_width: Width of the inductor track.
        :param bend_radius: Radius to be used for the curved corners
        :param signal_input_connection_x: X-coordinate of the signal
        input connection point, located at the center of the track at the
        midpoint of the inductor.
        :param signal_input_connection_y: Y-coordinate of the signal
        input connection point, located at the center of the track at the
        midpoint of the inductor.
        :param tolerance: Tolerance to define number of points
        approximating circle. Defaults to 0.01.
        """

        self.track_width = track_width
        self.bend_radius = bend_radius
        self.tolerance = tolerance
        self.signal_input_connection_x = signal_input_connection_x
        self.signal_input_connection_y = signal_input_connection_y
        self.separation = 14.0

    def make_cell(self, microstrip_layer: int, cell_name="Inductor") -> gdspy.Cell:
        """
        Returns the gdspy Cell for a given inductor instance. The default geometry is
        oriented such that the signal input to the inductor has the smallest y value and readout connection the highest.
        To add a rotation this must be done with the .CellReference() method after calling this method.
        """

        origin_x = self.signal_input_connection_x
        origin_y = self.signal_input_connection_y
        track_width = self.track_width
        bend_radius = self.bend_radius
        tolerance = self.tolerance
        separation = self.separation

        # Define the cell to add inductor geometries to.
        inductor_cell = gdspy.Cell(cell_name)

        # Defines points for the inductor path.
        inductor_points = [
            (origin_x, (origin_y + separation / 2 + track_width / 2)),
            (origin_x, (origin_y + 162)),
            ((origin_x + 40), (origin_y + 162)),
            ((origin_x + 40), (origin_y + track_width / 2 + separation / 2)),
            ((origin_x + 80), (origin_y + track_width / 2 + separation / 2)),
            ((origin_x + 80), (origin_y + track_width / 2 + separation / 2 + 217)),
        ]

        inductor_geometry = gdspy.FlexPath(
            inductor_points,
            width=[track_width, track_width],
            offset=separation + track_width,
            corners="circular bend",
            bend_radius=bend_radius,
            tolerance=tolerance,
            ends="flush",
            layer=microstrip_layer,
        )

        signal_in_overlap = gdspy.Rectangle(
            ((origin_x - 4.0), (origin_y + track_width / 2)),
            ((origin_x + 4.0), (origin_y - 8)),
            layer=microstrip_layer,
        )

        midpoint_geometry_1 = gdspy.FlexPath(
            [
                (origin_x, origin_y),
                ((origin_x + separation / 2 + track_width / 2), origin_y),
                (
                    (origin_x + separation / 2 + track_width / 2),
                    origin_y + separation / 2 + track_width / 2,
                ),
            ],
            width=track_width,
            corners="circular bend",
            bend_radius=bend_radius,
            tolerance=tolerance,
            ends="flush",
            layer=microstrip_layer,
        )
        midpoint_geometry_2 = gdspy.FlexPath(
            [
                (origin_x, origin_y),
                ((origin_x - separation / 2 - track_width / 2), origin_y),
                (
                    (origin_x - separation / 2 - track_width / 2),
                    origin_y + separation / 2 + track_width / 2,
                ),
            ],
            width=track_width,
            corners="circular bend",
            bend_radius=bend_radius,
            tolerance=tolerance,
            ends="flush",
            layer=microstrip_layer,
        )

        inductor_cell.add(
            [
                inductor_geometry,
                signal_in_overlap,
                midpoint_geometry_1,
                midpoint_geometry_2,
            ]
        )

        return inductor_cell

    def get_idc_connection_x(self):
        """
        Function to return the X-coordinate of the central point between the two
        ends that would connect to the IDC.
        """

        return self.signal_input_connection_x + 80

    def get_idc_connection_y(self):
        """
        Function to return the Y-coordinate of the central point between the two
        ends that would connect to the IDC.
        """

        origin_y = self.signal_input_connection_y
        track_width = self.track_width
        separation = self.separation

        return origin_y + track_width / 2 + separation / 2 + 217
