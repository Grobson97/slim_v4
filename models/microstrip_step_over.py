import gdspy
from gdspy.library import Cell
import numpy as np


class MicrostripStepOver:
    def __init__(
        self,
        microstrip_width: float,
        centre_x=0.0,
        centre_y=0.0,
    ) -> None:
        """
        Creates new instance of a step over with a via between two microstrip lines.

        :param float microstrip_width: Width of the microstrip lines at the inputs and outputs.
        :param float centre_x: X coordinate of the geometric centre of the cross over.
        :param float centre_y: Y coordinate of the geometric centre of the cross over.
        """

        self.microstrip_width = microstrip_width
        self.centre_x = centre_x
        self.centre_y = centre_y

        self.port_distance_from_centre = 100

    def make_cell(
        self,
        main_microstrip_layer: int,
        ground_layer: int,
        dielectric_bridge_layer: int,
        cell_name="Step Over",
    ) -> gdspy.Cell:
        """
        Returns the gdspy Cell for a MicrostripStepOver instance. The geometry is effectively a cross centred at the
        (centre_x, centre_y) coordinates, with inputs at the South (Input 1) and West (Input 2), and outputs at the
        North (Output 1) and East (Output 2). Where Input 1 to Output 1 is the straight path and Input 2 to Output 2 is
        the stepped path.

        :param int main_microstrip_layer: GDSII layer for the main microstrips (inputs and outputs).
        :param int ground_layer: GDSII layer for the microstrip bridge on the step over in the ground plane.
        :param int dielectric_bridge_layer: GDSII layer for the dielectric patch to bridge the step over.
        :param str cell_name: Name to be used to reference the cell.
        """

        origin_x = self.centre_x
        origin_y = self.centre_y

        port_distance_from_centre = self.port_distance_from_centre

        # Create cell to add coupler geometry to
        step_over_cell = gdspy.Cell(cell_name)

        stub_cell = gdspy.Cell(f"{cell_name} - Stub")

        stub_length = 6.5
        stub_cap_length = 1
        stub_max_line_width = 10
        stub_centre_offset = 2.5

        stub_vertices = [
            (0.0, 0.0),
            (0.0, self.microstrip_width / 2),
            (stub_length, stub_max_line_width / 2),
            (stub_length + stub_cap_length, stub_max_line_width / 2),
            (stub_length + stub_cap_length, -stub_max_line_width / 2),
            (stub_length, -stub_max_line_width / 2),
            (0.0, -self.microstrip_width / 2),
        ]

        stub_polygon = gdspy.Polygon(
            points=stub_vertices,
            layer=main_microstrip_layer,
        )
        stub_cell.add(stub_polygon)

        straight_path = gdspy.FlexPath(
            points=[
                (origin_x, origin_y - port_distance_from_centre),
                (origin_x, origin_y + port_distance_from_centre),
            ],
            width=self.microstrip_width,
            layer=main_microstrip_layer,
        )

        input_stepped_path = gdspy.FlexPath(
            points=[
                (origin_x - port_distance_from_centre, origin_y),
                (
                    origin_x - stub_centre_offset - stub_length - stub_cap_length,
                    origin_y,
                ),
            ],
            width=self.microstrip_width,
            layer=main_microstrip_layer,
        )

        output_stepped_path = gdspy.FlexPath(
            points=[
                (origin_x + port_distance_from_centre, origin_y),
                (
                    origin_x + stub_centre_offset + stub_length + stub_cap_length,
                    origin_y,
                ),
            ],
            width=self.microstrip_width,
            layer=main_microstrip_layer,
        )

        ground_plane_cut_length = 15
        ground_plane_cut_width = 6
        via_inset = 1

        ground_plane_hole = gdspy.Rectangle(
            (
                origin_x - ground_plane_cut_length / 2,
                origin_y + ground_plane_cut_width / 2,
            ),
            (
                origin_x + ground_plane_cut_length / 2,
                origin_y - ground_plane_cut_width / 2,
            ),
            layer=ground_layer,
        )

        stepped_path = gdspy.FlexPath(
            points=[
                (
                    origin_x - stub_centre_offset - via_inset - self.microstrip_width,
                    origin_y,
                ),
                (
                    origin_x + stub_centre_offset + via_inset + self.microstrip_width,
                    origin_y,
                ),
            ],
            width=self.microstrip_width,
            layer=ground_layer,
        )

        ground_plane_hole = gdspy.boolean(
            ground_plane_hole, stepped_path, "not", layer=ground_layer
        )

        via_hole_1 = gdspy.Rectangle(
            (
                origin_x - stub_centre_offset - via_inset - self.microstrip_width,
                origin_y + self.microstrip_width / 2,
            ),
            (
                origin_x - stub_centre_offset - via_inset,
                origin_y - self.microstrip_width / 2,
            ),
            layer=dielectric_bridge_layer,
        )
        via_hole_2 = gdspy.Rectangle(
            (
                origin_x + stub_centre_offset + via_inset + self.microstrip_width,
                origin_y + self.microstrip_width / 2,
            ),
            (
                origin_x + stub_centre_offset + via_inset,
                origin_y - self.microstrip_width / 2,
            ),
            layer=dielectric_bridge_layer,
        )

        # Add geometry to coupler cell.
        step_over_cell.add(
            [
                straight_path,
                input_stepped_path,
                output_stepped_path,
                via_hole_1,
                via_hole_2,
                ground_plane_hole,
                gdspy.CellReference(
                    stub_cell, origin=(stub_centre_offset, 0.0), rotation=0.0
                ),
                gdspy.CellReference(
                    stub_cell, origin=(-stub_centre_offset, 0.0), rotation=180
                ),
                gdspy.CellReference(
                    stub_cell, origin=(0.0, stub_centre_offset), rotation=90
                ),
                gdspy.CellReference(
                    stub_cell, origin=(0.0, -stub_centre_offset), rotation=-90
                ),
            ]
        )

        return step_over_cell

    def get_straight_input_connection(self) -> tuple:
        """
        Function to return the input coordinates of the straight path relative to the centre of the step over origin.
        """

        return (self.centre_x, self.centre_y - self.port_distance_from_centre)

    def get_straight_output_connection(self) -> tuple:
        """
        Function to return the output coordinates of the straight path relative to the centre of the step over origin.
        """

        return (self.centre_x, self.centre_y + self.port_distance_from_centre)

    def get_stepped_input_connection(self) -> tuple:
        """
        Function to return the input coordinates of the stepped path relative to the centre of the step over origin.
        """

        return (self.centre_x - self.port_distance_from_centre, self.centre_y)

    def get_stepped_output_connection(self) -> tuple:
        """
        Function to return the output coordinates of the stepped path relative to the centre of the step over origin.
        """

        return (self.centre_x + self.port_distance_from_centre, self.centre_y)
