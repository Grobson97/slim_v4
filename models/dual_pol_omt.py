import gdspy
import numpy as np
from models.omt import OMT
from models.microstrip_step_over import MicrostripStepOver
from models.hybrid_180 import Hybrid180


class DualPolOMT:
    def __init__(
        self,
        diameter: float,
        choke_width: float,
        outer_path_radius: float,
        centre_x=0.0,
        centre_y=0.0,
    ) -> None:
        """
        Creates a new instance of an orthomode transducer antenna. NB: All
        dimensions must be in micrometers.

        :param: diameter: Diameter of the OMT. The same diameter as the end of
        the feed horn.
        :param: choke_width: Distance between the inner and outer diameter of
        the choke.
        :param outer_path_radius: Value to use to move hybrid and step over origins further or closer to centre of OMT.
        It should be a little bigger than the diameter.
        :param: centre_x: x-coordinate of the centre of the OMT.
        :param: centre_y: x-coordinate of the centre of the OMT.
        """

        self.diameter = diameter
        self.choke_width = choke_width
        self.centre_x = centre_x
        self.centre_y = centre_y
        self.outer_path_radius = outer_path_radius

        self.microstrip_width = 2.0

        self.omt = OMT(
            diameter=self.diameter,
            choke_width=self.choke_width,
            dual_polarisation=True,
            centre_x=0.0,
            centre_y=0.0,
        )
        self.hybrid_180 = Hybrid180(
            resonator_length=600,
            loop_width=2.0,
            input_width=2.0,
            difference_port_width=2.5,
            sum_port_connection_x=0.0,
            sum_port_connection_y=0.0,
        )

        self.step_over = MicrostripStepOver(
            microstrip_width=self.microstrip_width,
            centre_x=0.0,
            centre_y=0.0,
        )

        self.omt_connections = self.omt.get_feedline_connections()

    def make_cell(
        self,
        microstrip_layer: int,
        ground_layer: int,
        step_over_layer: int,
        dielectric_layer: int,
        back_etch_layer: int,
        lossy_metal_layer: int,
        cell_name="Full Dual Pol OMT",
        tolerance=0.01,
    ) -> gdspy.Cell:
        """
        Returns the gdspy Cell for a given Dual pol OMT instance.

        :param microstrip_layer: GDSII layer for the antenna paddles.
        :param ground_layer: GDSII layer for the ground plane.
        :param step_over_layer: GDSII layer for the microstrip on the step over.
        :param dielectric_layer: GDSII layer for the dielectric.
        :param back_etch_layer: GDSII layer for the etching of the bulk wafer.
        :param  lossy_metal_layer: GDSII layer for the lossy metal connected to the sum port of the hybrid.
        :param cell_name: Name to be used to reference the cell.
        :param tolerance: Approximate curvature resolution. The number of
        points is automatically calculated.
        """

        radius = self.diameter / 2
        center_x = self.centre_x
        center_y = self.centre_y

        microstrip_width = 2.0

        full_omt_cell = gdspy.Cell(cell_name)

        # Add omt component.
        omt_cell = self.omt.make_cell(
            antenna_layer=microstrip_layer,
            ground_layer=ground_layer,
            dielectric_layer=dielectric_layer,
            back_etch_layer=back_etch_layer,
            cell_name=cell_name + " - OMT",
        )

        step_over_cell = self.step_over.make_cell(
            main_microstrip_layer=microstrip_layer,
            ground_layer=step_over_layer,
            dielectric_bridge_layer=dielectric_layer,
            cell_name="Step Over",
        )

        real_step_over_origin = (
            self.outer_path_radius / np.sqrt(2) + 300,
            self.outer_path_radius / np.sqrt(2) + 300,
        )
        real_step_over_cell = gdspy.CellReference(
            step_over_cell,
            origin=real_step_over_origin,
            rotation=0.0,
        )

        # Dummy Step over for the western path
        dummy_step_over_1_origin = (
            -self.outer_path_radius / np.sqrt(2) - 300,
            self.outer_path_radius / np.sqrt(2) + 300,
        )
        dummy_step_over_1_cell = gdspy.CellReference(
            step_over_cell,
            origin=dummy_step_over_1_origin,
            rotation=0.0,
        )

        # Dummy Step over for the southern path
        dummy_step_over_2_origin = (
            self.outer_path_radius / np.sqrt(2) + 300,
            -self.outer_path_radius / np.sqrt(2) - 300,
        )
        dummy_step_over_2_cell = gdspy.CellReference(
            step_over_cell,
            origin=dummy_step_over_2_origin,
            rotation=0.0,
        )

        step_over_straight_input = self.step_over.get_straight_input_connection()
        step_over_stepped_input = self.step_over.get_stepped_input_connection()
        step_over_straight_output = self.step_over.get_straight_output_connection()
        step_over_stepped_output = self.step_over.get_stepped_output_connection()

        ########################################################################################################################

        # Section to add feedlines from OMT to step over:

        north_path_points_1 = [
            (self.omt_connections[0][0], self.omt_connections[0][1]),
            (
                    self.omt_connections[0][0] + 100 * np.sin((15 / 360) * 2 * np.pi),
                    self.omt_connections[0][1] + 100 * np.cos((15 / 360) * 2 * np.pi)
            ),
            (
                step_over_stepped_input[0] + real_step_over_origin[0] - 50,
                step_over_stepped_input[1] + real_step_over_origin[1],
            ),
            (
                step_over_stepped_input[0] + real_step_over_origin[0],
                step_over_stepped_input[1] + real_step_over_origin[1],
            ),
        ]
        north_path_1 = gdspy.FlexPath(
            points=north_path_points_1,
            width=[microstrip_width, 100.0],
            corners="circular bend",
            bend_radius=100,
            layer=[microstrip_layer, 100],
        )

        east_path_points_1 = [
            (self.omt_connections[2][0], self.omt_connections[2][1]),
            (
                self.omt_connections[2][0] + 100 * np.cos((15 / 360) * 2 * np.pi),
                self.omt_connections[2][1] + 100 * np.sin((15 / 360) * 2 * np.pi)
            ),
            (
                step_over_straight_input[0] + real_step_over_origin[0],
                step_over_straight_input[1] + real_step_over_origin[1] - 50,
            ),
            (
                step_over_straight_input[0] + real_step_over_origin[0],
                step_over_straight_input[1] + real_step_over_origin[1],
            ),
        ]
        east_path_1 = gdspy.FlexPath(
            points=east_path_points_1,
            width=[microstrip_width, 100.0],
            corners="circular bend",
            bend_radius=100,
            layer=[microstrip_layer, 100],
        )

        south_path_points_1 = [
            (self.omt_connections[1][0], self.omt_connections[1][1]),
            (
                self.omt_connections[1][0] + 100 * np.sin((15 / 360) * 2 * np.pi),
                self.omt_connections[1][1] - 100 * np.cos((15 / 360) * 2 * np.pi)
            ),            (
                step_over_stepped_input[0] + dummy_step_over_2_origin[0] - 50,
                step_over_stepped_input[1] + dummy_step_over_2_origin[1],
            ),
            (
                step_over_stepped_input[0] + dummy_step_over_2_origin[0],
                step_over_stepped_input[1] + dummy_step_over_2_origin[1],
            ),
        ]
        south_path_1 = gdspy.FlexPath(
            points=south_path_points_1,
            width=[microstrip_width, 100.0],
            corners="circular bend",
            bend_radius=100,
            layer=[microstrip_layer, 100],
        )

        west_path_points_1 = [
            (self.omt_connections[3][0], self.omt_connections[3][1]),
            (
                self.omt_connections[3][0] - 100 * np.cos((15 / 360) * 2 * np.pi),
                self.omt_connections[3][1] + 100 * np.sin((15 / 360) * 2 * np.pi)
            ),            (
                step_over_straight_input[0] + dummy_step_over_1_origin[0],
                step_over_straight_input[1] + dummy_step_over_1_origin[1] - 50,
            ),
            (
                step_over_straight_input[0] + dummy_step_over_1_origin[0],
                step_over_straight_input[1] + dummy_step_over_1_origin[1],
            ),
        ]
        west_path_1 = gdspy.FlexPath(
            points=west_path_points_1,
            width=[microstrip_width, 100.0],
            corners="circular bend",
            bend_radius=100,
            layer=[microstrip_layer, 100],
        )

        ########################################################################################################################

        # Section to add hybrid 180's North and East of the OMT.

        hybrid_180_north_origin = (0.0, self.outer_path_radius + 200)
        hybrid_180_east_origin = (self.outer_path_radius + 200, 0.0)

        hybrid_180_cell = self.hybrid_180.make_cell(
            microstrip_layer=microstrip_layer, cell_name="Hybrid 180"
        )

        hybrid_180_input_1_connection = self.hybrid_180.get_input_connection_A()
        hybrid_180_input_2_connection = self.hybrid_180.get_input_connection_B()

        # **************************************************************************************************************

        # Section to add lossy line to hybrid:

        lossy_path_cell = gdspy.Cell(cell_name + "lossy path")

        lossy_path_points = [
            (hybrid_180_north_origin[0], hybrid_180_north_origin[1]),
            (hybrid_180_north_origin[0], hybrid_180_north_origin[1] - 50),
            (hybrid_180_north_origin[0] + 200, hybrid_180_north_origin[1] - 50),
            (hybrid_180_north_origin[0] + 200, hybrid_180_north_origin[1] - 100),
            (hybrid_180_north_origin[0] - 325, hybrid_180_north_origin[1] - 100),
            (hybrid_180_north_origin[0] - 325, hybrid_180_north_origin[1] - 150),
            (hybrid_180_north_origin[0] + 450, hybrid_180_north_origin[1] - 150),
            (hybrid_180_north_origin[0] + 450, hybrid_180_north_origin[1] - 200),
            (hybrid_180_north_origin[0] - 575, hybrid_180_north_origin[1] - 200),
            (hybrid_180_north_origin[0] - 575, hybrid_180_north_origin[1] - 250),
            (hybrid_180_north_origin[0] + 600, hybrid_180_north_origin[1] - 250),
            (hybrid_180_north_origin[0] + 600, hybrid_180_north_origin[1] - 300),
            (hybrid_180_north_origin[0] - 725, hybrid_180_north_origin[1] - 300),
            (hybrid_180_north_origin[0] - 725, hybrid_180_north_origin[1] - 350),
            (hybrid_180_north_origin[0] + 850, hybrid_180_north_origin[1] - 350),
            (hybrid_180_north_origin[0] + 850, hybrid_180_north_origin[1] - 400),
            (hybrid_180_north_origin[0] - 975, hybrid_180_north_origin[1] - 400),
            (hybrid_180_north_origin[0] - 975, hybrid_180_north_origin[1] - 450),
            (hybrid_180_north_origin[0] + 1100, hybrid_180_north_origin[1] - 450),
        ]

        lossy_path = gdspy.FlexPath(
            points=lossy_path_points,
            width=3.0,
            bend_radius=30.0,
            layer=lossy_metal_layer,
            corners="circular bend"
        )

        lossy_path_cell.add(lossy_path)
        full_omt_cell.add(gdspy.CellReference(lossy_path_cell))
        full_omt_cell.add(gdspy.CellReference(lossy_path_cell, rotation=-90))

        ########################################################################################################################

        # Section to add feedline between step over and hybrid

        # Define path widths to make tapered transition leading up to hybrid to ensure 3um inputs.
        north_path_points_2 = [
            (
                step_over_stepped_output[0] + real_step_over_origin[0],
                step_over_stepped_output[1] + real_step_over_origin[1],
            ),
            (
                step_over_stepped_output[0] + real_step_over_origin[0] + 100,
                step_over_stepped_output[1] + real_step_over_origin[1],
            ),
            (
                hybrid_180_input_1_connection[0] + hybrid_180_east_origin[0],
                hybrid_180_input_1_connection[1] + hybrid_180_east_origin[1] + 100,
            ),
            (
                hybrid_180_input_1_connection[0] + hybrid_180_east_origin[0],
                hybrid_180_input_1_connection[1] + hybrid_180_east_origin[1],
            ),
        ]
        north_path_2 = gdspy.FlexPath(
            points=north_path_points_2,
            width=microstrip_width,
            corners="circular bend",
            bend_radius=100,
            layer=microstrip_layer,
        )

        east_path_points_2 = [
            (
                step_over_straight_output[0] + real_step_over_origin[0],
                step_over_straight_output[1] + real_step_over_origin[1],
            ),
            (
                step_over_straight_output[0] + real_step_over_origin[0],
                step_over_straight_output[1] + real_step_over_origin[1] + 100,
            ),
            (
                hybrid_180_input_1_connection[1] + hybrid_180_north_origin[0] + 100,
                hybrid_180_input_1_connection[0] + hybrid_180_north_origin[1],
            ),
            (
                hybrid_180_input_1_connection[1] + hybrid_180_north_origin[0],
                hybrid_180_input_1_connection[0] + hybrid_180_north_origin[1],
            ),
        ]
        east_path_2 = gdspy.FlexPath(
            points=east_path_points_2,
            width=microstrip_width,
            corners="circular bend",
            bend_radius=100,
            layer=microstrip_layer,
        )

        south_path_points_2 = [
            (
                step_over_stepped_output[0] + dummy_step_over_2_origin[0],
                step_over_stepped_output[1] + dummy_step_over_2_origin[1],
            ),
            (
                step_over_stepped_output[0] + dummy_step_over_2_origin[0] + 100,
                step_over_stepped_output[1] + dummy_step_over_2_origin[1],
            ),
            (
                hybrid_180_input_2_connection[0] + hybrid_180_east_origin[0],
                hybrid_180_input_2_connection[1] + hybrid_180_east_origin[1] - 100,
            ),
            (
                hybrid_180_input_2_connection[0] + hybrid_180_east_origin[0],
                hybrid_180_input_2_connection[1] + hybrid_180_east_origin[1],
            ),
        ]
        south_path_2 = gdspy.FlexPath(
            points=south_path_points_2,
            width=microstrip_width,
            corners="circular bend",
            bend_radius=100,
            layer=microstrip_layer,
        )

        west_path_points_2 = [
            (
                step_over_straight_output[0] + dummy_step_over_1_origin[0],
                step_over_straight_output[1] + dummy_step_over_1_origin[1],
            ),
            (
                step_over_straight_output[0] + dummy_step_over_1_origin[0],
                step_over_straight_output[1] + dummy_step_over_1_origin[1] + 100,
            ),
            (
                hybrid_180_input_2_connection[1] + hybrid_180_north_origin[0] - 100,
                hybrid_180_input_2_connection[0] + hybrid_180_north_origin[1],
            ),
            (
                hybrid_180_input_2_connection[1] + hybrid_180_north_origin[0],
                hybrid_180_input_2_connection[0] + hybrid_180_north_origin[1],
            ),
        ]

        west_path_2 = gdspy.FlexPath(
            points=west_path_points_2,
            width=microstrip_width,
            corners="circular bend",
            bend_radius=100,
            layer=microstrip_layer,
        )

        ################################################################################################################

        # Section to add lossy termination lines to dummy step overs:

        dummy_termination_cell = gdspy.Cell(cell_name + "Dummy Termination")

        dummy_termination_path_points = [
            (0.0, -10.0),
            (0.0, 40.0),
            (150.0, 40.0),
            (150.0, 80.0),
            (-150.0, 80.0),
            (-150.0, 120.0),
            (150.0, 120.0),
            (150.0, 160.0),
            (-150.0, 160.0),
            (-150.0, 200.0),
            (150.0, 200.0),
            (150.0, 240.0),
            (-150.0, 240.0),
            (-150.0, 280.0),
            (150.0, 280.0),
            (150.0, 320.0),
            (-150.0, 320.0),
            (-150.0, 360.0),
            (150.0, 360.0),
        ]

        dummy_termination_overlap = gdspy.Polygon(
            [
                (0.0, -20.0),
                (1.0, -20.0),
                (4.0, -5.0),
                (4.0, 0.0),
                (-4.0, 0.0),
                (-4.0, -5.0),
                (-1.0, -20.0),
                (0.0, -20.0),
            ],
            layer=microstrip_layer
        )

        dummy_termination_path = gdspy.FlexPath(
            points=dummy_termination_path_points,
            width=2.0,
            bend_radius=20.0,
            layer=lossy_metal_layer,
            corners="circular bend"
        )

        dummy_termination_cell.add([dummy_termination_path, dummy_termination_overlap])
        full_omt_cell.add(
            [
                gdspy.CellReference(
                    dummy_termination_cell,
                    origin=(dummy_step_over_1_origin[0] + step_over_stepped_input[0],
                            dummy_step_over_1_origin[1] + step_over_stepped_input[1]),
                    rotation=90),
                gdspy.CellReference(
                    dummy_termination_cell,
                    origin=(dummy_step_over_1_origin[0] + step_over_stepped_output[0],
                            dummy_step_over_1_origin[1] + step_over_stepped_output[1]),
                    rotation=-90),
                gdspy.CellReference(
                    dummy_termination_cell,
                    origin=(dummy_step_over_2_origin[0] + step_over_straight_input[0],
                            dummy_step_over_2_origin[1] + step_over_straight_input[1]),
                    rotation=180),
                gdspy.CellReference(
                    dummy_termination_cell,
                    origin=(dummy_step_over_2_origin[0] + step_over_straight_output[0],
                            dummy_step_over_2_origin[1] + step_over_straight_output[1]),
                    rotation=0.0),
            ]
        )

        full_omt_cell.add(
            [
                gdspy.CellReference(omt_cell, origin=(center_x, center_y), rotation=0),
                real_step_over_cell,
                dummy_step_over_1_cell,
                dummy_step_over_2_cell,
                north_path_1,
                east_path_1,
                south_path_1,
                west_path_1,
                north_path_2,
                east_path_2,
                south_path_2,
                west_path_2,
                gdspy.CellReference(
                    hybrid_180_cell, origin=hybrid_180_north_origin, rotation=90
                ),
                gdspy.CellReference(
                    hybrid_180_cell, origin=hybrid_180_east_origin, rotation=0
                ),
            ]
        )

        dielectric_path_polygons = full_omt_cell.get_polygons(by_spec=(100, 0))
        dielectric_membrane_negative = full_omt_cell.get_polygons(by_spec=(99, 0))

        dielectric_membrane_negative = gdspy.boolean(
            dielectric_membrane_negative,
            dielectric_path_polygons,
            "not",
            layer=dielectric_layer
        )

        full_omt_cell.add(dielectric_membrane_negative)
        full_omt_cell.remove_polygons(lambda pts, layer, datatype: layer == 99)
        full_omt_cell.remove_polygons(lambda pts, layer, datatype: layer == 100)

        return full_omt_cell

    def get_connections(self) -> list:
        """
        Function to return the x and y coordinates of the connection points to the difference ports of the north and
        eastern hybrid 180's. Outputs a list containing two tuples [(x1, y1), (x2, y2)] where x1 and y1 are the north
        connection and x2 and y2 are the coordinates for the eastern connection.
        """

        hybrid_difference_port_connection = (
            self.hybrid_180.get_difference_port_connection()
        )

        connections = [
            (
                -hybrid_difference_port_connection[1],
                hybrid_difference_port_connection[0] + self.outer_path_radius + 200,
            ),
            (
                hybrid_difference_port_connection[0] + self.outer_path_radius + 200,
                hybrid_difference_port_connection[1],
            ),
        ]

        return connections
