import itertools
import math

import gdspy
import numpy as np
import random
import pandas as pd

from matplotlib import pyplot as plt

import util.filter_bank_builder_tools
import util.filter_bank_builder_tools as tools
from models.cpw_coupler_stub import CPWCouplerStub
from models.cpw_stub import CPWStub
from models.filter_bank import FilterBank
from models.lekid import Lekid
from models.pp_lekid import PPLekid
from util import filter_bank_builder_tools, general_gds_tools


def make_single_spectrometer_cell(
    lekid_f0_array: np.ndarray,
    filter_bank: FilterBank,
    spectrometer_cell: gdspy.Cell,
    readout_width: float,
    readout_gap: float,
    layers: dict,
    parallel_plate=False,
    step_down=True,
):
    """
    Function to add a single spectrometer geometry with connected lekids to a desired cell with the connection to the
    filter-bank feedline at (0,0). This will return the updated spectrometer_cell and the two spectrometer readout
    connection coordinates, where readout_connection_1 is the upper side, and readout_connection_2 is the lower side.

    :param lekid_f0_array: Array of f0's for the LEKIDS in the spectrometer.
    :param filter_bank: Instance of the filter-bank to which the LEKIDS are connected.
    :param spectrometer_cell: Cell to add the spectrometer to.
    :param spectrometer_origin: origin of the spectrometer.
    :param readout_width: Width of the CPW readout line.
    :param readout_gap: Gap of the CPW readout line.
    :param layers: Dictionary of the GDSII layers, must have the following keys: "back_etch", "niobium_microstrip",
    "aluminium_microstrip", "sin":, "niobium_ground", "wafer",
    :param parallel_plate: Boolean to use parallel plates as detectors. If False, IDC's will be used.
    :param step_down: Boolean to build IDC lekids with a membrane step down.

    :return: spectrometer_cell, readout_connection_1, readout_connection_2.
    """

    filter_bank_cell = filter_bank.make_cell(
        microstrip_layer=layers["mm-feedline"],
        ground_layer=layers["ground"],
        cell_name=spectrometer_cell.name + " Filter Bank",
    )

    spectrometer_cell.add(filter_bank_cell)

    lekid_connection_array = filter_bank.get_lekid_connections()

    # Find largest lekid in the spectrometer to use to set the total length of each lekid in spectrometer.
    max_capacitor_length = tools.lekid_idc_length(np.min(lekid_f0_array), step_down=step_down)
    max_coupler_length = tools.lekid_coupler_length(max_capacitor_length, step_down=step_down)
    # if parallel_plate:
    # max_capacitor_length = tools.lekid_ppcap_length(np.min(lekid_f0_array))
    # max coupler_length = tools.ppcap_lekid_coupler_length(max_capacitor_length)

    readout_connections = [[], []]

    top_step_down_points = []
    bottom_step_down_points = []

    lekid_names = []
    used_lekid_f0_array = []

    for count, connection in enumerate(lekid_connection_array):
        # Create instance of current connections lekid for each spectrometer:
        lekid = Lekid(
            target_f0=lekid_f0_array[count],
            step_down=step_down,
            signal_input_x=0.0,
            signal_input_y=0.0,
        )
        if parallel_plate:
            lekid = PPLekid(
                target_f0=lekid_f0_array[count],
                signal_input_x=0.0,
                signal_input_y=0.0,
            )

        # Calculate length required to extend signal in connection so LEKID readout line is straight for spectrometer.
        if not parallel_plate:
            extra_signal_in_length = (
                (max_capacitor_length + max_coupler_length)
                - lekid.idc_length
                - lekid.coupler_length
            )
        if parallel_plate:
            extra_signal_in_length = (
                max_capacitor_length + max_coupler_length
            ) - lekid.capacitor_length

        rotation_radians = 0
        list_index = 0  # variable to change which connection array the current lekid is added to. Allows two sub arrays
        # per spectrometer, one for the upper side and one the lower.

        # Add vertex for step down.
        if connection[2] == "up":
            top_step_down_points.append(
                (connection[0] - 115, connection[1] + extra_signal_in_length + 255)
            )
            top_step_down_points.append(
                (connection[0] + 134, connection[1] + extra_signal_in_length + 255)
            )

        if connection[2] == "down":
            rotation_radians = np.pi
            list_index = 1
            extra_signal_in_length = (
                -extra_signal_in_length
            )  # make length minus if lekid is down.

            # Add vertex for step down.
            bottom_step_down_points.append(
                (connection[0] - 134, connection[1] + extra_signal_in_length - 255)
            )
            bottom_step_down_points.append(
                (connection[0] + 115, connection[1] + extra_signal_in_length - 255)
            )

        rotation_degrees = (rotation_radians / (2 * np.pi)) * 360

        # Join filter channel output to LEKID inductor input:
        filter_to_inductor_path = gdspy.FlexPath(
            points=[
                (connection[0], connection[1]),
                (connection[0], connection[1] + extra_signal_in_length),
            ],
            width=3.0,
            layer=layers["mm-feedline"],
        )

        # Rotate coordinates of LEKID readout depending on which side of the FBS the LEKID is.
        lekid_readout_coordinates_input = tools.rotate_coordinates(
            lekid.get_readout_connections(
                readout_width=readout_width,
            )[
                0
            ][0],
            lekid.get_readout_connections(
                readout_width=readout_width,
            )[
                0
            ][1],
            angle=rotation_radians,
        )
        lekid_readout_coordinates_output = tools.rotate_coordinates(
            lekid.get_readout_connections(
                readout_width=readout_width,
            )[
                1
            ][0],
            lekid.get_readout_connections(
                readout_width=readout_width,
            )[
                1
            ][1],
            angle=rotation_radians,
        )

        # Add LEKID connections to readout list relative to the origin of the filter-bank.
        readout_connections[list_index].append(
            [
                [
                    0.0 + connection[0] + lekid_readout_coordinates_input[0],
                    0.0
                    + connection[1]
                    + extra_signal_in_length
                    + lekid_readout_coordinates_input[1],
                ],
                [
                    0.0 + connection[0] + lekid_readout_coordinates_output[0],
                    0.0
                    + connection[1]
                    + extra_signal_in_length
                    + lekid_readout_coordinates_output[1],
                ],
            ]
        )

        if not parallel_plate:
            # Make cells for each lekid:
            lekid_cell = lekid.make_cell(
                inductor_layer=layers["inductors"],
                idc_layer=layers["idc"],
                readout_layer=layers["readout_centre"],
                ground_layer=layers["ground"],
                dielectric_layer=layers["dielectric"],
                nitride_step_down_layer=layers["nitride_step_down"],
                oxide_step_down_layer=layers["oxide_step_down"],
                e_beam_boundary_layer=layers["e_beam_boundary"],
                readout_width=readout_width,
                readout_gap=readout_gap,
                cell_name=spectrometer_cell.name + str(count),
            )

        if parallel_plate:
            lekid_cell = lekid.make_cell(
                inductor_layer=layers["inductors"],
                device_plate_layer=layers["capacitor_plates"],
                readout_layer=layers["readout_centre"],
                ground_layer=layers["ground"],
                readout_width=readout_width,
                readout_gap=readout_gap,
                cell_name=spectrometer_cell.name + str(count),
            )

        lekid_reference = gdspy.CellReference(
            lekid_cell,
            (connection[0], connection[1] + extra_signal_in_length),
            rotation=rotation_degrees,
        )

        spectrometer_cell.add([lekid_reference, filter_to_inductor_path])

        lekid_names.append(f"{spectrometer_cell.name[40:] + str(count)}")
        used_lekid_f0_array.append(str(lekid_f0_array[count]))

    # Add coordinates for the vertices on the readout side.

    top_step_down_extra_points = [
        (top_step_down_points[-1][0] + 500, top_step_down_points[-1][1]),
        (top_step_down_points[-1][0] + 500, bottom_step_down_points[-1][1]),
        (readout_connections[0][-1][1][0] + 1500, bottom_step_down_points[-1][1]),
        (
            readout_connections[0][-1][1][0] + 1500,
            readout_connections[0][-1][1][1] + 300,
        ),
        (0 - 1000, readout_connections[0][0][0][1] + 300),
    ]

    bottom_step_down_extra_points = [
        (readout_connections[0][-1][1][0] + 1500, bottom_step_down_points[-1][1]),
        (
            readout_connections[0][-1][1][0] + 1500,
            readout_connections[1][-1][0][1] - 300,
        ),
        (readout_connections[1][-1][0][0] - 29, readout_connections[1][-1][0][1] - 300),
        (0 - 1000, readout_connections[1][0][1][1] - 300),
    ]

    top_step_down_points += top_step_down_extra_points
    bottom_step_down_points += bottom_step_down_extra_points

    # create polygon for step down points:
    top_step_down_polygon = gdspy.Polygon(
        top_step_down_points, layer=layers["oxide_step_down"]
    )
    bottom_step_down_polygon = gdspy.Polygon(
        bottom_step_down_points, layer=layers["oxide_step_down"]
    )

    if not parallel_plate and step_down:
        spectrometer_cell.add(top_step_down_polygon)
        spectrometer_cell.add(bottom_step_down_polygon)

    connect_spectrometer_readouts(
        readout_connections=readout_connections,
        readout_width=readout_width,
        readout_gap=readout_gap,
        spectrometer_cell=spectrometer_cell,
        layers=layers,
    )

    spectrometer_readout_connection_1 = (
        readout_connections[0][0][0][0],
        readout_connections[0][0][0][1],
    )
    spectrometer_readout_connection_2 = (
        readout_connections[1][0][1][0],
        readout_connections[1][0][1][1],
    )

    # Save lekid names and f0's:
    lekid_names = np.array(lekid_names, dtype=str)
    used_lekid_f0_array = np.array(used_lekid_f0_array, dtype=str)

    df = pd.DataFrame({"LEKID Tag": lekid_names, "F0": used_lekid_f0_array})
    df.to_csv(f"{spectrometer_cell.name[:41]}_f0_scheduling.txt", index=False, sep=",")

    return (
        spectrometer_cell,
        spectrometer_readout_connection_1,
        spectrometer_readout_connection_2,
    )


def make_duo_spectrometer_cell(
    spectrometer_a_origin: tuple,
    spectrometer_b_origin: tuple,
    lekid_f0_array_a: np.ndarray,
    lekid_f0_array_b: np.ndarray,
    filter_bank: FilterBank,
    spectrometer_group_cell: gdspy.Cell,
    readout_width: float,
    readout_gap: float,
    layers: dict,
):
    """
        Function to add a quad spectrometer geometry (A group of 2 spectrometers, A, B stacked vertically, where
        A's origin is the highest in y and B's the lowest) to the desired spectrometer_group_cell. This will return the
        updated spectrometer_group_cell and the coordinates for the connection at the upper side of spectrometer A, and
        the bottom side of spectrometer B.

        :param spectrometer_a_origin: Origin of spectrometer A (The connection of the filter-bank feedline).
        :param spectrometer_b_origin: Origin of spectrometer B (The connection of the filter-bank feedline).
        :param lekid_f0_array_a:
        :param lekid_f0_array_b: Array of f0's for the LEKIDS in spectrometer B.
        :param filter_bank:
        :param spectrometer_group_cell: Cell to add the four spectrometers to.
        :param readout_width: Width of the CPW readout line.
        :param readout_gap: Gap of the CPW readout line.
        :param layers: Dictionary of the GDSII layers, must have the following keys: "back_etch", "niobium_microstrip",
        "aluminium_microstrip", "sin":, "niobium_ground", "wafer",
    }
        :return: spectrometer_group_cell, readout_connection_1_a, readout_connection_b_2
    """

    # Create cells for each of the spectrometers (filter bank plus LEKID's)
    spectrometer_cell_a = gdspy.Cell("Duo Spectrometer A")
    spectrometer_cell_b = gdspy.Cell("Duo Spectrometer B")

    # Build each spectrometer:
    (
        spectrometer_cell_a,
        readout_connection_1_a,
        readout_connection_2_a,
    ) = make_single_spectrometer_cell(
        lekid_f0_array=lekid_f0_array_a,
        filter_bank=filter_bank,
        spectrometer_cell=spectrometer_cell_a,
        readout_width=readout_width,
        readout_gap=readout_gap,
        layers=layers,
    )
    (
        spectrometer_cell_b,
        readout_connection_1_b,
        readout_connection_2_b,
    ) = make_single_spectrometer_cell(
        lekid_f0_array=lekid_f0_array_b,
        filter_bank=filter_bank,
        spectrometer_cell=spectrometer_cell_b,
        readout_width=readout_width,
        readout_gap=readout_gap,
        layers=layers,
    )

    # Make reference cells:
    spectrometer_reference_a = gdspy.CellReference(
        spectrometer_cell_a, spectrometer_a_origin
    )
    spectrometer_reference_b = gdspy.CellReference(
        spectrometer_cell_b, spectrometer_b_origin
    )

    # Add to group cell:
    spectrometer_group_cell.add(
        [
            spectrometer_reference_a,
            spectrometer_reference_b,
        ]
    )
    spectrometer_connections = np.array(
        [
            np.array(readout_connection_1_a) + np.array(spectrometer_a_origin),
            np.array(readout_connection_2_a) + np.array(spectrometer_a_origin),
            np.array(readout_connection_1_b) + np.array(spectrometer_b_origin),
            np.array(readout_connection_2_b) + np.array(spectrometer_b_origin),
        ]
    )

    connect_spectrometers(
        spectrometer_connections=spectrometer_connections,
        readout_width=readout_width,
        readout_gap=readout_gap,
        spectrometer_group_cell=spectrometer_group_cell,
        layers=layers,
    )

    right_hand_vertex_x = filter_bank.get_lekid_connections()[-2][0] + 1500
    middle_step_down_points = [
        (
            spectrometer_a_origin[0] - 1000,
            readout_connection_2_a[1] + spectrometer_a_origin[1],
        ),
        (
            spectrometer_a_origin[0] - 1000,
            readout_connection_1_b[1] + spectrometer_b_origin[1],
        ),
        (right_hand_vertex_x, readout_connection_1_b[1] + spectrometer_b_origin[1]),
        (right_hand_vertex_x, readout_connection_2_a[1] + spectrometer_a_origin[1]),
    ]

    # create polygon for step down points:
    middle_step_down_polygon = gdspy.Polygon(
        middle_step_down_points, layer=layers["oxide_step_down"]
    )
    spectrometer_group_cell.add(middle_step_down_polygon)

    return (
        spectrometer_group_cell,
        spectrometer_connections[0],
        spectrometer_connections[-1],
    )


def make_trio_spectrometer_cell(
    spectrometer_a_origin: tuple,
    spectrometer_b_origin: tuple,
    spectrometer_c_origin: tuple,
    lekid_f0_array_a: np.ndarray,
    lekid_f0_array_b: np.ndarray,
    lekid_f0_array_c: np.ndarray,
    filter_bank: FilterBank,
    spectrometer_group_cell: gdspy.Cell,
    readout_width: float,
    readout_gap: float,
    layers: dict,
):
    """
        Function to add a quad spectrometer geometry (A group of 3 spectrometers, A, B, C, stacked vertically, where
        A's origin is the highest in y and C's the lowest) to the desired spectrometer_group_cell. This will return the
        updated spectrometer_group_cell and the coordinates for the connection at the upper side of spectrometer A, and
        the bottom side of spectrometer C.

        :param spectrometer_a_origin: Origin of spectrometer A (The connection of the filter-bank feedline).
        :param spectrometer_b_origin: Origin of spectrometer B (The connection of the filter-bank feedline).
        :param spectrometer_c_origin: Origin of spectrometer C (The connection of the filter-bank feedline).
        :param lekid_f0_array_a:
        :param lekid_f0_array_b: Array of f0's for the LEKIDS in spectrometer B.
        :param lekid_f0_array_c: Array of f0's for the LEKIDS in spectrometer B.
        :param filter_bank:
        :param spectrometer_group_cell: Cell to add the four spectrometers to.
        :param readout_width: Width of the CPW readout line.
        :param readout_gap: Gap of the CPW readout line.
        :param layers: Dictionary of the GDSII layers, must have the following keys: "back_etch", "niobium_microstrip",
        "aluminium_microstrip", "sin":, "niobium_ground", "wafer",
    }
        :return: spectrometer_group_cell, readout_connection_1_a, readout_connection_c_2
    """

    # Create cells for each of the spectrometers (filter bank plus LEKID's)
    spectrometer_cell_a = gdspy.Cell("Trio Spectrometer A")
    spectrometer_cell_b = gdspy.Cell("Trio Spectrometer B")
    spectrometer_cell_c = gdspy.Cell("Trio Spectrometer C")

    # Build each spectrometer:
    (
        spectrometer_cell_a,
        readout_connection_1_a,
        readout_connection_2_a,
    ) = make_single_spectrometer_cell(
        lekid_f0_array=lekid_f0_array_a,
        filter_bank=filter_bank,
        spectrometer_cell=spectrometer_cell_a,
        readout_width=readout_width,
        readout_gap=readout_gap,
        layers=layers,
    )
    (
        spectrometer_cell_b,
        readout_connection_1_b,
        readout_connection_2_b,
    ) = make_single_spectrometer_cell(
        lekid_f0_array=lekid_f0_array_b,
        filter_bank=filter_bank,
        spectrometer_cell=spectrometer_cell_b,
        readout_width=readout_width,
        readout_gap=readout_gap,
        layers=layers,
    )
    (
        spectrometer_cell_c,
        readout_connection_1_c,
        readout_connection_2_c,
    ) = make_single_spectrometer_cell(
        lekid_f0_array=lekid_f0_array_c,
        filter_bank=filter_bank,
        spectrometer_cell=spectrometer_cell_c,
        readout_width=readout_width,
        readout_gap=readout_gap,
        layers=layers,
    )

    # Make reference cells:
    spectrometer_reference_a = gdspy.CellReference(
        spectrometer_cell_a, spectrometer_a_origin
    )
    spectrometer_reference_b = gdspy.CellReference(
        spectrometer_cell_b, spectrometer_b_origin
    )
    spectrometer_reference_c = gdspy.CellReference(
        spectrometer_cell_c, spectrometer_c_origin
    )

    # Add to group cell:
    spectrometer_group_cell.add(
        [
            spectrometer_reference_a,
            spectrometer_reference_b,
            spectrometer_reference_c,
        ]
    )
    spectrometer_connections = np.array(
        [
            np.array(readout_connection_1_a) + np.array(spectrometer_a_origin),
            np.array(readout_connection_2_a) + np.array(spectrometer_a_origin),
            np.array(readout_connection_1_b) + np.array(spectrometer_b_origin),
            np.array(readout_connection_2_b) + np.array(spectrometer_b_origin),
            np.array(readout_connection_1_c) + np.array(spectrometer_c_origin),
            np.array(readout_connection_2_c) + np.array(spectrometer_c_origin),
        ]
    )

    connect_spectrometers(
        spectrometer_connections=spectrometer_connections,
        readout_width=readout_width,
        readout_gap=readout_gap,
        spectrometer_group_cell=spectrometer_group_cell,
        layers=layers,
    )

    # Create step down polygon between spectrometers:
    right_hand_vertex_x = filter_bank.get_lekid_connections()[-2][0] + 1500
    middle_step_down_1_points = [
        (
            spectrometer_a_origin[0] - 1000,
            readout_connection_2_a[1] + spectrometer_a_origin[1],
        ),
        (
            spectrometer_a_origin[0] - 1000,
            readout_connection_1_b[1] + spectrometer_b_origin[1],
        ),
        (right_hand_vertex_x, readout_connection_1_b[1] + spectrometer_b_origin[1]),
        (right_hand_vertex_x, readout_connection_2_a[1] + spectrometer_a_origin[1]),
    ]
    middle_step_down_2_points = [
        (
            spectrometer_b_origin[0] - 1000,
            readout_connection_2_b[1] + spectrometer_b_origin[1],
        ),
        (
            spectrometer_c_origin[0] - 1000,
            readout_connection_1_c[1] + spectrometer_c_origin[1],
        ),
        (right_hand_vertex_x, readout_connection_1_c[1] + spectrometer_c_origin[1]),
        (right_hand_vertex_x, readout_connection_2_b[1] + spectrometer_b_origin[1]),
    ]

    # create polygon for step down points:
    middle_step_down_1_polygon = gdspy.Polygon(
        middle_step_down_1_points, layer=layers["oxide_step_down"]
    )
    middle_step_down_2_polygon = gdspy.Polygon(
        middle_step_down_2_points, layer=layers["oxide_step_down"]
    )
    spectrometer_group_cell.add(middle_step_down_1_polygon)
    spectrometer_group_cell.add(middle_step_down_2_polygon)

    return (
        spectrometer_group_cell,
        spectrometer_connections[0],
        spectrometer_connections[-1],
    )


def make_quad_spectrometer_cell(
    spectrometer_a_origin: tuple,
    spectrometer_b_origin: tuple,
    spectrometer_c_origin: tuple,
    spectrometer_d_origin: tuple,
    lekid_f0_array_a: np.ndarray,
    lekid_f0_array_b: np.ndarray,
    lekid_f0_array_c: np.ndarray,
    lekid_f0_array_d: np.ndarray,
    filter_bank: FilterBank,
    spectrometer_group_cell: gdspy.Cell,
    readout_width: float,
    readout_gap: float,
    layers: dict,
):
    """
        Function to add a quad spectrometer geometry (A group of 4 spectrometers, A, B, C, D stacked vertically, where
        A's origin is the highest in y and D's the lowest) to the desired spectrometer_group_cell. This will return the
        updated spectrometer_group_cell and the coordinates for the connection at the upper side of spectrometer A, and
        the bottom side of spectrometer D.

        :param spectrometer_a_origin: Origin of spectrometer A (The connection of the filter-bank feedline).
        :param spectrometer_b_origin: Origin of spectrometer B (The connection of the filter-bank feedline).
        :param spectrometer_c_origin: Origin of spectrometer C (The connection of the filter-bank feedline).
        :param spectrometer_d_origin: Origin of spectrometer D (The connection of the filter-bank feedline).
        :param lekid_f0_array_a:
        :param lekid_f0_array_b: Array of f0's for the LEKIDS in spectrometer B.
        :param lekid_f0_array_c: Array of f0's for the LEKIDS in spectrometer B.
        :param lekid_f0_array_d: Array of f0's for the LEKIDS in spectrometer B.
        :param filter_bank:
        :param spectrometer_group_cell: Cell to add the four spectrometers to.
        :param readout_width: Width of the CPW readout line.
        :param readout_gap: Gap of the CPW readout line.
        :param layers: Dictionary of the GDSII layers, must have the following keys: "back_etch", "niobium_microstrip",
        "aluminium_microstrip", "sin":, "niobium_ground", "wafer",
    }
        :return: spectrometer_group_cell, readout_connection_1_a, readout_connection_d_2
    """

    # Create cells for each of the spectrometers (filter bank plus LEKID's)
    spectrometer_cell_a = gdspy.Cell("Quad Spectrometer A")
    spectrometer_cell_b = gdspy.Cell("Quad Spectrometer B")
    spectrometer_cell_c = gdspy.Cell("Quad Spectrometer C")
    spectrometer_cell_d = gdspy.Cell("Quad Spectrometer D")

    # Build each spectrometer:
    (
        spectrometer_cell_a,
        readout_connection_1_a,
        readout_connection_2_a,
    ) = make_single_spectrometer_cell(
        lekid_f0_array=lekid_f0_array_a,
        filter_bank=filter_bank,
        spectrometer_cell=spectrometer_cell_a,
        readout_width=readout_width,
        readout_gap=readout_gap,
        layers=layers,
    )
    (
        spectrometer_cell_b,
        readout_connection_1_b,
        readout_connection_2_b,
    ) = make_single_spectrometer_cell(
        lekid_f0_array=lekid_f0_array_b,
        filter_bank=filter_bank,
        spectrometer_cell=spectrometer_cell_b,
        readout_width=readout_width,
        readout_gap=readout_gap,
        layers=layers,
    )
    (
        spectrometer_cell_c,
        readout_connection_1_c,
        readout_connection_2_c,
    ) = make_single_spectrometer_cell(
        lekid_f0_array=lekid_f0_array_c,
        filter_bank=filter_bank,
        spectrometer_cell=spectrometer_cell_c,
        readout_width=readout_width,
        readout_gap=readout_gap,
        layers=layers,
    )
    (
        spectrometer_cell_d,
        readout_connection_1_d,
        readout_connection_2_d,
    ) = make_single_spectrometer_cell(
        lekid_f0_array=lekid_f0_array_d,
        filter_bank=filter_bank,
        spectrometer_cell=spectrometer_cell_d,
        readout_width=readout_width,
        readout_gap=readout_gap,
        layers=layers,
    )
    # Make reference cells:
    spectrometer_reference_a = gdspy.CellReference(
        spectrometer_cell_a, spectrometer_a_origin
    )
    spectrometer_reference_b = gdspy.CellReference(
        spectrometer_cell_b, spectrometer_b_origin
    )
    spectrometer_reference_c = gdspy.CellReference(
        spectrometer_cell_c, spectrometer_c_origin
    )
    spectrometer_reference_d = gdspy.CellReference(
        spectrometer_cell_d, spectrometer_d_origin
    )

    # Add to group cell:
    spectrometer_group_cell.add(
        [
            spectrometer_reference_a,
            spectrometer_reference_b,
            spectrometer_reference_c,
            spectrometer_reference_d,
        ]
    )
    spectrometer_connections = np.array(
        [
            np.array(readout_connection_1_a) + np.array(spectrometer_a_origin),
            np.array(readout_connection_2_a) + np.array(spectrometer_a_origin),
            np.array(readout_connection_1_b) + np.array(spectrometer_b_origin),
            np.array(readout_connection_2_b) + np.array(spectrometer_b_origin),
            np.array(readout_connection_1_c) + np.array(spectrometer_c_origin),
            np.array(readout_connection_2_c) + np.array(spectrometer_c_origin),
            np.array(readout_connection_1_d) + np.array(spectrometer_d_origin),
            np.array(readout_connection_2_d) + np.array(spectrometer_d_origin),
        ]
    )

    connect_spectrometers(
        spectrometer_connections=spectrometer_connections,
        readout_width=readout_width,
        readout_gap=readout_gap,
        spectrometer_group_cell=spectrometer_group_cell,
        layers=layers,
    )

    return (
        spectrometer_group_cell,
        spectrometer_connections[0],
        spectrometer_connections[-1],
    )


def make_radial_duo_spectrometer_cell(
    filter_bank_radius: float,
    filter_bank_rotation_angle: float,
    lekid_f0_array_a: np.ndarray,
    lekid_f0_array_b: np.ndarray,
    filter_bank: FilterBank,
    cell_name: str,
    readout_width: float,
    readout_gap: float,
    bridge_width: float,
    layers: dict,
    parallel_plate=False,
    step_down=True,
):
    """
        Function to add a radial Duo spectrometer geometry (A group of 3 spectrometers, A, B arranged radially
        relative to an origin, where each FBS mm-connection (origin) is a given radius away from the origin.
        where spectrometer A is rotated the furthest in the anti-clockwise direction, B is the furthest rotated in the
        clockwise direction.

        :param filter_bank_radius: Radius at which to place the start of the filter banks.
        :param filter_bank_rotation_angle: The difference in rotation angle about the origin between neighbouring FBS's
        in degrees.
        :param lekid_f0_array_a: Array of f0's for the LEKIDS in spectrometer A.
        :param lekid_f0_array_b: Array of f0's for the LEKIDS in spectrometer B.
        :param filter_bank: FilterBank object.
        :param cell_name: Name for group cell.
        :param readout_width: Width of the CPW readout line.
        :param readout_gap: Gap of the CPW readout line.
        :param bridge_width: Width of bridges parallel to path of CPW.
        :param layers: Dictionary of the GDSII layers, must have the following keys: "back_etch", "niobium_microstrip",
        "aluminium_microstrip", "sin":, "niobium_ground", "wafer",
        :param parallel_plate: Boolean to use parallel plates for detectors. If False, IDC's will be used.
        :param step_down: Boolean to make IDC LEKID with step down layers or not.
    }
        :return: (
            spectrometer_group_cell,
            spectrometer_origin_a,
            spectrometer_origin_b,
            readout_connection_1_a,
            readout_connection_2_b
        )
    """
    spectrometer_group_cell = gdspy.Cell(name=cell_name)

    # Create cells for each of the spectrometers (filter bank plus LEKID's)
    spectrometer_cell_a = gdspy.Cell(cell_name + " Spectrometer #A")
    spectrometer_cell_b = gdspy.Cell(cell_name + " Spectrometer #B")

    # Build each spectrometer:
    (
        spectrometer_cell_a,
        readout_connection_1_a,
        readout_connection_2_a,
    ) = make_single_spectrometer_cell(
        lekid_f0_array=lekid_f0_array_a,
        filter_bank=filter_bank,
        spectrometer_cell=spectrometer_cell_a,
        readout_width=readout_width,
        readout_gap=readout_gap,
        layers=layers,
        parallel_plate=parallel_plate,
        step_down=step_down,
    )
    (
        spectrometer_cell_b,
        readout_connection_1_b,
        readout_connection_2_b,
    ) = make_single_spectrometer_cell(
        lekid_f0_array=lekid_f0_array_b,
        filter_bank=filter_bank,
        spectrometer_cell=spectrometer_cell_b,
        readout_width=readout_width,
        readout_gap=readout_gap,
        layers=layers,
        parallel_plate=parallel_plate,
        step_down=step_down,
    )

    # Move spectrometers to the desired radius so they can then be rotated about a common origin.
    translated_spectrometer_cell_a = gdspy.Cell(cell_name + " Translated FBS Cell A")
    translated_spectrometer_cell_b = gdspy.Cell(cell_name + " Translated FBS Cell B")

    translated_spectrometer_cell_a.add(
        gdspy.CellReference(spectrometer_cell_a, origin=(filter_bank_radius, 0.0))
    )
    translated_spectrometer_cell_b.add(
        gdspy.CellReference(spectrometer_cell_b, origin=(filter_bank_radius, 0.0))
    )

    # Make reference cells:
    spectrometer_reference_a = gdspy.CellReference(
        translated_spectrometer_cell_a,
        (0.0, 0.0),
        rotation=filter_bank_rotation_angle / 2,
    )
    spectrometer_reference_b = gdspy.CellReference(
        translated_spectrometer_cell_b,
        (0.0, 0.0),
        rotation=-filter_bank_rotation_angle / 2,
    )

    # Add to group cell:
    spectrometer_group_cell.add(
        [
            spectrometer_reference_a,
            spectrometer_reference_b,
        ]
    )

    rotation_angle_radians = (filter_bank_rotation_angle / 360) * 2 * np.pi
    # Rotate origins and connections accordingly:
    (
        spectrometer_a_origin_x,
        spectrometer_a_origin_y,
    ) = util.filter_bank_builder_tools.rotate_coordinates(
        x=filter_bank_radius, y=0.0, angle=rotation_angle_radians / 2
    )
    (
        readout_connection_1_a_x,
        readout_connection_1_a_y,
    ) = util.filter_bank_builder_tools.rotate_coordinates(
        x=filter_bank_radius + readout_connection_1_a[0],
        y=0.0 + readout_connection_1_a[1],
        angle=rotation_angle_radians / 2,
    )
    (
        readout_connection_2_a_x,
        readout_connection_2_a_y,
    ) = util.filter_bank_builder_tools.rotate_coordinates(
        x=filter_bank_radius + readout_connection_2_a[0],
        y=0.0 + readout_connection_2_a[1],
        angle=rotation_angle_radians / 2,
    )
    (
        spectrometer_b_origin_x,
        spectrometer_b_origin_y,
    ) = util.filter_bank_builder_tools.rotate_coordinates(
        x=filter_bank_radius, y=0.0, angle=-rotation_angle_radians / 2
    )
    (
        readout_connection_1_b_x,
        readout_connection_1_b_y,
    ) = util.filter_bank_builder_tools.rotate_coordinates(
        x=filter_bank_radius + readout_connection_1_b[0],
        y=0.0 + readout_connection_1_b[1],
        angle=-rotation_angle_radians / 2,
    )
    (
        readout_connection_2_b_x,
        readout_connection_2_b_y,
    ) = util.filter_bank_builder_tools.rotate_coordinates(
        x=filter_bank_radius + readout_connection_2_b[0],
        y=0.0 + readout_connection_2_b[1],
        angle=-rotation_angle_radians / 2,
    )

    # ******************************************************************************************************************

    # Connect readout between spectrometers:

    dx = 750
    dy = dx * np.tan(rotation_angle_radians / 2)
    connection_points = [
        (readout_connection_2_a_x, readout_connection_2_a_y),
        (readout_connection_2_a_x - dx, readout_connection_2_a_y - dy),
        (readout_connection_1_b_x - dx, readout_connection_1_b_y + dy),
        (readout_connection_1_b_x, readout_connection_1_b_y),
    ]

    connecting_readout_path = general_gds_tools.build_cpw_with_bridges(
        path_points=connection_points,
        cpw_centre_width=readout_width,
        cpw_gap=readout_gap,
        bend_radius=200,
        bridge_thickness=bridge_width,
        bridge_separation=500.0,
        cpw_centre_layer=layers["readout_centre"],
        ground_layer=layers["ground"],
        cell_name="Duo inter-FBS readout",
    )

    if not parallel_plate and step_down:
        # Add step down geometry around microwave path:
        connecting_readout_path_step_down = gdspy.FlexPath(
            points=connection_points, width=1000, layer=layers["oxide_step_down"]
        )

        spectrometer_group_cell.add(connecting_readout_path_step_down)
    spectrometer_group_cell.add(connecting_readout_path)

    return (
        spectrometer_group_cell,
        [spectrometer_a_origin_x, spectrometer_a_origin_y],
        [spectrometer_b_origin_x, spectrometer_b_origin_y],
        [readout_connection_1_a_x, readout_connection_1_a_y],
        [readout_connection_2_b_x, readout_connection_2_b_y],
    )


def make_radial_trio_spectrometer_cell(
    filter_bank_radius: float,
    filter_bank_rotation_angle: float,
    lekid_f0_array_a: np.ndarray,
    lekid_f0_array_b: np.ndarray,
    lekid_f0_array_c: np.ndarray,
    filter_bank: FilterBank,
    cell_name: str,
    readout_width: float,
    readout_gap: float,
    bridge_width: float,
    layers: dict,
    parallel_plate: False,
):
    """
        Function to add a radial trio spectrometer geometry (A group of 3 spectrometers, A, B, C, arranged radially
        relative to an origin, where each FBS mm-connection (origin) is a given radius away from the origin.
        where spectrometer A is rotated the furthest in the anti-clockwise direction, B is in the middle and C is the
        furthest rotated in the clockwise direction.

        :param filter_bank_radius: Radius at which to place the start of the filter banks.
        :param filter_bank_rotation_angle: The difference in rotation angle about the origin between neighbouring FBS's.
        :param lekid_f0_array_a: Array of f0's for the LEKIDS in spectrometer A.
        :param lekid_f0_array_b: Array of f0's for the LEKIDS in spectrometer B.
        :param lekid_f0_array_c: Array of f0's for the LEKIDS in spectrometer C.
        :param filter_bank: FilterBank object.
        :param cell_name: Name for group cell.
        :param readout_width: Width of the CPW readout line.
        :param readout_gap: Gap of the CPW readout line.
        :param bridge_width: Width of bridges parallel to path of CPW.
        :param layers: Dictionary of the GDSII layers, must have the following keys: "back_etch", "niobium_microstrip",
        "aluminium_microstrip", "sin":, "niobium_ground", "wafer",
        :param parallel_plate: Boolean to use parallel plates for detectors. If False, IDC's will be used.
    }
        :return: (
            spectrometer_group_cell,
            spectrometer_origin_a,
            spectrometer_origin_b,
            spectrometer_origin_c
            readout_connection_1_a,
            readout_connection_2_c
        )
    """
    spectrometer_group_cell = gdspy.Cell(name=cell_name)

    # Create cells for each of the spectrometers (filter bank plus LEKID's)
    spectrometer_cell_a = gdspy.Cell(cell_name + " Spectrometer #A")
    spectrometer_cell_b = gdspy.Cell(cell_name + " Spectrometer #B")
    spectrometer_cell_c = gdspy.Cell(cell_name + " Spectrometer #C")

    # Build each spectrometer:
    (
        spectrometer_cell_a,
        readout_connection_1_a,
        readout_connection_2_a,
    ) = make_single_spectrometer_cell(
        lekid_f0_array=lekid_f0_array_a,
        filter_bank=filter_bank,
        spectrometer_cell=spectrometer_cell_a,
        readout_width=readout_width,
        readout_gap=readout_gap,
        layers=layers,
        parallel_plate=parallel_plate,
    )
    (
        spectrometer_cell_b,
        readout_connection_1_b,
        readout_connection_2_b,
    ) = make_single_spectrometer_cell(
        lekid_f0_array=lekid_f0_array_b,
        filter_bank=filter_bank,
        spectrometer_cell=spectrometer_cell_b,
        readout_width=readout_width,
        readout_gap=readout_gap,
        layers=layers,
        parallel_plate=parallel_plate,
    )
    (
        spectrometer_cell_c,
        readout_connection_1_c,
        readout_connection_2_c,
    ) = make_single_spectrometer_cell(
        lekid_f0_array=lekid_f0_array_c,
        filter_bank=filter_bank,
        spectrometer_cell=spectrometer_cell_c,
        readout_width=readout_width,
        readout_gap=readout_gap,
        layers=layers,
        parallel_plate=parallel_plate,
    )

    # Move spectrometers to the desired radius so they can then be rotated about a common origin.
    translated_spectrometer_cell_a = gdspy.Cell(cell_name + " Translated FBS Cell A")
    translated_spectrometer_cell_b = gdspy.Cell(cell_name + " Translated FBS Cell B")
    translated_spectrometer_cell_c = gdspy.Cell(cell_name + " Translated FBS Cell C")

    translated_spectrometer_cell_a.add(
        gdspy.CellReference(spectrometer_cell_a, origin=(filter_bank_radius, 0.0))
    )
    translated_spectrometer_cell_b.add(
        gdspy.CellReference(spectrometer_cell_b, origin=(filter_bank_radius, 0.0))
    )
    translated_spectrometer_cell_c.add(
        gdspy.CellReference(spectrometer_cell_c, origin=(filter_bank_radius, 0.0))
    )

    # Make reference cells:
    spectrometer_reference_a = gdspy.CellReference(
        translated_spectrometer_cell_a,
        (0.0, 0.0),
        rotation=filter_bank_rotation_angle,
    )
    spectrometer_reference_b = gdspy.CellReference(
        translated_spectrometer_cell_b,
        (0.0, 0.0),
        rotation=0,
    )
    spectrometer_reference_c = gdspy.CellReference(
        translated_spectrometer_cell_c,
        (0.0, 0.0),
        rotation=-filter_bank_rotation_angle,
    )

    # Add to group cell:
    spectrometer_group_cell.add(
        [
            spectrometer_reference_a,
            spectrometer_reference_b,
            spectrometer_reference_c,
        ]
    )

    rotation_angle_radians = (filter_bank_rotation_angle / 360) * 2 * np.pi
    # Rotate origins and connections accordingly:
    (
        spectrometer_a_origin_x,
        spectrometer_a_origin_y,
    ) = util.filter_bank_builder_tools.rotate_coordinates(
        x=filter_bank_radius, y=0.0, angle=rotation_angle_radians
    )
    (
        readout_connection_1_a_x,
        readout_connection_1_a_y,
    ) = util.filter_bank_builder_tools.rotate_coordinates(
        x=filter_bank_radius + readout_connection_1_a[0],
        y=0.0 + readout_connection_1_a[1],
        angle=rotation_angle_radians,
    )
    (
        readout_connection_2_a_x,
        readout_connection_2_a_y,
    ) = util.filter_bank_builder_tools.rotate_coordinates(
        x=filter_bank_radius + readout_connection_2_a[0],
        y=0.0 + readout_connection_2_a[1],
        angle=rotation_angle_radians,
    )
    (
        spectrometer_b_origin_x,
        spectrometer_b_origin_y,
    ) = util.filter_bank_builder_tools.rotate_coordinates(
        x=filter_bank_radius, y=0.0, angle=0.0
    )
    (
        readout_connection_1_b_x,
        readout_connection_1_b_y,
    ) = util.filter_bank_builder_tools.rotate_coordinates(
        x=filter_bank_radius + readout_connection_1_b[0],
        y=0.0 + readout_connection_1_b[1],
        angle=0.0,
    )
    (
        readout_connection_2_b_x,
        readout_connection_2_b_y,
    ) = util.filter_bank_builder_tools.rotate_coordinates(
        x=filter_bank_radius + readout_connection_2_b[0],
        y=0.0 + readout_connection_2_b[1],
        angle=0.0,
    )
    (
        spectrometer_c_origin_x,
        spectrometer_c_origin_y,
    ) = util.filter_bank_builder_tools.rotate_coordinates(
        x=filter_bank_radius, y=0.0, angle=-rotation_angle_radians
    )
    (
        readout_connection_1_c_x,
        readout_connection_1_c_y,
    ) = util.filter_bank_builder_tools.rotate_coordinates(
        x=filter_bank_radius + readout_connection_1_c[0],
        y=0.0 + readout_connection_1_c[1],
        angle=-rotation_angle_radians,
    )
    (
        readout_connection_2_c_x,
        readout_connection_2_c_y,
    ) = util.filter_bank_builder_tools.rotate_coordinates(
        x=filter_bank_radius + readout_connection_2_c[0],
        y=0.0 + readout_connection_2_c[1],
        angle=-rotation_angle_radians,
    )

    # ******************************************************************************************************************

    # Connect readout between spectrometers:
    dx = 750
    dy = dx * np.tan(rotation_angle_radians)
    hypotenuse = np.sqrt(dx**2 + dy**2)
    connection_points_1 = [
        (readout_connection_2_a_x, readout_connection_2_a_y),
        (readout_connection_2_a_x - dx, readout_connection_2_a_y - dy),
        (readout_connection_1_b_x - hypotenuse, readout_connection_1_b_y),
        (readout_connection_1_b_x, readout_connection_1_b_y),
    ]

    connecting_readout_path_1 = general_gds_tools.build_cpw_with_bridges(
        path_points=connection_points_1,
        cpw_centre_width=readout_width,
        cpw_gap=readout_gap,
        bend_radius=200,
        bridge_thickness=bridge_width,
        bridge_separation=500.0,
        cpw_centre_layer=layers["readout_centre"],
        ground_layer=layers["ground"],
        cell_name=cell_name + "inter-FBS readout 1",
    )
    connection_points_2 = [
        (readout_connection_2_b_x, readout_connection_2_b_y),
        (readout_connection_2_b_x - hypotenuse, readout_connection_2_b_y),
        (readout_connection_1_c_x - dx, readout_connection_1_c_y + dy),
        (readout_connection_1_c_x, readout_connection_1_c_y),
    ]
    connecting_readout_path_2 = general_gds_tools.build_cpw_with_bridges(
        path_points=connection_points_2,
        cpw_centre_width=readout_width,
        cpw_gap=readout_gap,
        bend_radius=200,
        bridge_thickness=bridge_width,
        bridge_separation=500.0,
        cpw_centre_layer=layers["readout_centre"],
        ground_layer=layers["ground"],
        cell_name=cell_name + "inter-FBS readout 2",
    )
    spectrometer_group_cell.add(connecting_readout_path_1)
    spectrometer_group_cell.add(connecting_readout_path_2)

    if not parallel_plate:
        # Add step down geometry around microwave path:
        connecting_readout_path_step_down_1 = gdspy.FlexPath(
            points=connection_points_1, width=1000, layer=layers["oxide_step_down"]
        )
        connecting_readout_path_step_down_2 = gdspy.FlexPath(
            points=connection_points_2, width=1000, layer=layers["oxide_step_down"]
        )
        spectrometer_group_cell.add(connecting_readout_path_step_down_1)
        spectrometer_group_cell.add(connecting_readout_path_step_down_2)

    return (
        spectrometer_group_cell,
        [spectrometer_a_origin_x, spectrometer_a_origin_y],
        [spectrometer_b_origin_x, spectrometer_b_origin_y],
        [spectrometer_c_origin_x, spectrometer_c_origin_y],
        [readout_connection_1_a_x, readout_connection_1_a_y],
        [readout_connection_2_c_x, readout_connection_2_c_y],
    )


def connect_spectrometer_readouts(
    readout_connections: list,
    readout_width: float,
    readout_gap: float,
    spectrometer_cell: gdspy.Cell,
    layers: dict,
):
    """
    Function to connect all the LEKID readouts together in a spectrometer. Readout geometry is added to the spectrometer
    cell. Does not return anything.

    :param readout_connections: Array of lekid readout connections, must have the format:
    [Upper/lower][LEKID][input/output][x/y].
    :param readout_width: Width of readout CPW transmission line.
    :param readout_gap: Gap size of readout CPW transmission line.
    :param spectrometer_cell: Spectrometer cell to add the connections to.
    :param layers: Dictionary of the GDSII layers, must have the following keys: "back_etch", "niobium_microstrip",
    "aluminium_microstrip", "sin":, "niobium_ground", "wafer",
    :return:
    """
    # Section to connect all the Lekid readout connections together.

    for count, connection in enumerate(readout_connections[0]):
        if connection != readout_connections[0][-1]:
            tools.cpw_connect_parallel_lines(
                x1=connection[1][0],
                y1=connection[1][1],
                x2=readout_connections[0][count + 1][0][0],
                y2=readout_connections[0][count + 1][0][1],
                centre_width=readout_width,
                ground_gap=readout_gap,
                bend_radius=readout_width * 3,
                centre_layer=layers["readout_centre"],
                ground_layer=layers["ground"],
                target_cell=spectrometer_cell,
                parallel_in_x=True,
            )
    for count, connection in enumerate(readout_connections[1]):
        if connection != readout_connections[1][-1]:
            tools.cpw_connect_parallel_lines(
                x1=connection[0][0],
                y1=connection[0][1],
                x2=readout_connections[1][count + 1][1][0],
                y2=readout_connections[1][count + 1][1][1],
                centre_width=readout_width,
                ground_gap=readout_gap,
                bend_radius=readout_width * 3,
                centre_layer=layers["readout_centre"],
                ground_layer=layers["ground"],
                target_cell=spectrometer_cell,
                parallel_in_x=True,
            )

    # Connecting final lekid on upper row of spectrometer to final lekid on the lower row.
    tools.cpw_connect_points(
        x1=readout_connections[0][-1][1][0],
        y1=readout_connections[0][-1][1][1],
        x2=readout_connections[0][-1][1][0] + 1000,
        y2=0.0,
        centre_width=readout_width,
        ground_gap=readout_gap,
        bend_radius=200,
        centre_layer=layers["readout_centre"],
        ground_layer=layers["ground"],
        target_cell=spectrometer_cell,
        x_first=True,
        add_bridges=True,
        bridge_thickness=18.864,
    )
    tools.cpw_connect_points(
        x1=readout_connections[0][-1][1][0] + 1000,
        y1=0.0,
        x2=readout_connections[1][-1][0][0],
        y2=readout_connections[1][-1][0][1],
        centre_width=readout_width,
        ground_gap=readout_gap,
        bend_radius=200,
        centre_layer=layers["readout_centre"],
        ground_layer=layers["ground"],
        target_cell=spectrometer_cell,
        x_first=False,
        add_bridges=True,
        bridge_thickness=18.864,
    )


def connect_spectrometers(
    spectrometer_connections: np.ndarray,
    readout_width: float,
    readout_gap: float,
    spectrometer_group_cell: gdspy.Cell,
    layers: dict,
):
    """
    Function to connect the cpw readout lines of vertically stacked spectrometers.

    :param spectrometer_connections: Array of spectrometer connection coordinates in the format: [input_A, output_A,
    input_B, output_B, input_C, output_C, input_D, output_D...]
    :param readout_width: Width of readout CPW transmission line.
    :param readout_gap: Gap size of readout CPW transmission line.
    :param spectrometer_group_cell:  Spectrometer group cell to add the connections to.
    :param layers: Dictionary of the GDSII layers, must have the following keys: "back_etch", "niobium_microstrip",
    "aluminium_microstrip", "sin":, "niobium_ground", "wafer",
    :return:
    """

    for count, connection in enumerate(spectrometer_connections):
        if count != 0 and tools.is_even(count):
            tools.cpw_connect_points(
                x1=spectrometer_connections[count - 1][0],
                y1=spectrometer_connections[count - 1][1],
                x2=spectrometer_connections[count - 1][0] - 500,
                y2=(spectrometer_connections[count - 1][1] + connection[1]) / 2,
                centre_width=readout_width,
                ground_gap=readout_gap,
                bend_radius=200,
                centre_layer=layers["readout_centre"],
                ground_layer=layers["ground"],
                target_cell=spectrometer_group_cell,
                x_first=True,
            )
            tools.cpw_connect_points(
                x1=spectrometer_connections[count - 1][0] - 500,
                y1=(spectrometer_connections[count - 1][1] + connection[1]) / 2,
                x2=connection[0],
                y2=connection[1],
                centre_width=readout_width,
                ground_gap=readout_gap,
                bend_radius=200,
                centre_layer=layers["readout_centre"],
                ground_layer=layers["ground"],
                target_cell=spectrometer_group_cell,
                x_first=False,
            )


def build_wave_spaced_stub_cascade(
    f0_array: np.ndarray,
    wavelength_spacing: float,
    stub_length: float,
    readout_width: float,
    readout_gap: float,
    cell_name: str,
    layers: dict,
):
    """
    Function to build a line of cpw stubs spaced a specified fraction of a wavelength apart based on a given f0 array.
    Returns the cell of the stub line. The cell produced is a cpw line extending from (0, 0) on the left out along the
    x-axis with stubs hanging below the line."

    :param f0_array: Array of f0's to define wavelength spacing between stubs. Each f0 defines the length of cpw
    transmission line after the current stub.
    :param wavelength_spacing: Fraction of wavelength to space between stubs.
    :param stub_length: Length that stub extends away from readout line.
    :param readout_width: Width of readout cpw center line.
    :param readout_gap: Gap of readout cpw line.
    :param cell_name: Cell name for cascaded stub cell.
    :param layers: Dictionary of the GDSII layers, must have the following keys: "back_etch", "niobium_microstrip",
    "aluminium_microstrip", "sin":, "niobium_ground", "wafer",
    :return: stub_cascade_cell, length of cpw line.
    """

    stub_cascade_cell = gdspy.Cell(name=cell_name)

    readout_connection_x = 0.0
    readout_connection_y = 0.0
    for count, f0 in enumerate(f0_array):
        # Build stub cell
        cpw_stub = CPWStub(
            cpw_centre_width=readout_width,
            cpw_gap=readout_gap,
            stub_length=stub_length,
            stub_width=6.0,
            readout_connection_x=readout_connection_x,
            readout_connection_y=readout_connection_y,
        )

        cpw_stub_cell = cpw_stub.make_cell(
            cpw_center_layer=layers["niobium_microstrip"],
            ground_layer=layers["niobium_ground"],
            cell_name=cell_name + " stub: " + str(count),
        )

        cpw_stub_ref_cell = gdspy.CellReference(cpw_stub_cell, origin=(0.0, 0.0))

        stub_cascade_cell.add(cpw_stub_ref_cell)

        # Calculate interconnection length required.
        wavelength = tools.convert_f0_to_physical_wavelength(f0)
        full_interconnection_length = wavelength * wavelength_spacing
        corrected_length = (
            full_interconnection_length - 140
        )  # Adjust length to account for length in stub cell.

        # Build interconnection
        tools.cpw_connect_parallel_lines(
            x1=cpw_stub.get_readout_output_connection_x(),
            y1=cpw_stub.get_readout_output_connection_y(),
            x2=cpw_stub.get_readout_output_connection_x() + corrected_length,
            y2=cpw_stub.get_readout_output_connection_y(),
            centre_width=readout_width,
            ground_gap=readout_gap,
            bend_radius=5 * readout_width,
            centre_layer=layers["niobium_microstrip"],
            ground_layer=layers["niobium_ground"],
            target_cell=stub_cascade_cell,
            parallel_in_x=True,
        )

        readout_connection_x = (
            cpw_stub.get_readout_output_connection_x() + corrected_length,
        )
        readout_connection_x = readout_connection_x[0]

    return stub_cascade_cell, readout_connection_x


def build_wave_spaced_coupler_stub_cascade(
    f0_array: np.ndarray,
    wavelength_spacing: float,
    coupler_length: float,
    readout_width: float,
    readout_gap: float,
    cell_name: str,
    layers: dict,
):
    """
    Function to build a line of cpw coupler stubs spaced a specified fraction of a wavelength apart based on a given f0
    array.\n
    Returns the cell of the stub line. The cell produced is a cpw line extending from (0, 0) on the left out along the
    x-axis with stubs hanging below the line."

    :param f0_array: Array of f0's to define wavelength spacing between stubs. Each f0 defines the length of cpw
    transmission line after the current stub.
    :param wavelength_spacing: Fraction of wavelength to space between stubs.
    :param coupler_length: Length of coupler.
    :param readout_width: Width of readout cpw center line.
    :param readout_gap: Gap of readout cpw line.
    :param cell_name: Cell name for cascaded stub cell.
    :param layers: Dictionary of the GDSII layers, must have the following keys: "back_etch", "niobium_microstrip",
    "aluminium_microstrip", "sin":, "niobium_ground", "wafer",
    :return: stub_cascade_cell, length of cpw line.
    """

    stub_cascade_cell = gdspy.Cell(name=cell_name)

    readout_connection_x = 0.0
    readout_connection_y = 0.0
    for count, f0 in enumerate(f0_array):
        # Build stub cell
        cpw_coupler_stub = CPWCouplerStub(
            cpw_centre_width=readout_width,
            cpw_gap=readout_gap,
            coupler_length=coupler_length,
            readout_connection_x=readout_connection_x,
            readout_connection_y=readout_connection_y,
        )

        cpw_coupler_stub_cell = cpw_coupler_stub.make_cell(
            cpw_center_layer=layers["readout_centre"],
            ground_layer=layers["ground"],
            dielectric_layer=layers["dielectric"],
            cell_name=cell_name + " stub: " + str(count),
        )

        cpw_stub_ref_cell = gdspy.CellReference(
            cpw_coupler_stub_cell, origin=(0.0, 0.0)
        )

        stub_cascade_cell.add(cpw_stub_ref_cell)

        # Calculate interconnection length required.
        wavelength = tools.convert_f0_to_physical_wavelength(f0)
        full_interconnection_length = wavelength * wavelength_spacing
        corrected_length = (
            full_interconnection_length - 140
        )  # Adjust length to account for length in stub cell.

        if count != len(f0_array) - 1:
            # Build interconnection
            tools.cpw_connect_parallel_lines(
                x1=cpw_coupler_stub.get_readout_output_connection_x(),
                y1=cpw_coupler_stub.get_readout_output_connection_y(),
                x2=cpw_coupler_stub.get_readout_output_connection_x()
                + corrected_length,
                y2=cpw_coupler_stub.get_readout_output_connection_y(),
                centre_width=readout_width,
                ground_gap=readout_gap,
                bend_radius=5 * readout_width,
                centre_layer=layers["readout_centre"],
                ground_layer=layers["ground"],
                target_cell=stub_cascade_cell,
                parallel_in_x=True,
            )

            readout_connection_x = (readout_connection_x + full_interconnection_length,)

        if count == len(f0_array) - 1:
            readout_connection_x = (readout_connection_x + 140,)

        readout_connection_x = readout_connection_x[0]

    return stub_cascade_cell, readout_connection_x


def build_wave_spaced_lekid_cascade(
    mm_f0_array: np.ndarray,
    lekid_f0_array: np.ndarray,
    wavelength_spacing: float,
    readout_width: float,
    readout_gap: float,
    cell_name: str,
    layers: dict,
    pp_cap_lekids=False,
    al_feedline=False,
    al_lekid=False,
):
    """
    Function to build a line of cpw coupled LEKIDS spaced a specified fraction of a wavelength apart based on a given
    mm-wave f0 array.
    Returns the cell of the line of LEKIDs. The cell produced is a cpw line extending from (0, 0) on the left out along
    the x-axis with LEKIDs hanging below the line."

    :param mm_f0_array: Array of f0's to define wavelength spacing between stubs. Each f0 defines the length of cpw
    transmission line after the current stub.
    :param lekid_f0_array: Array of f0's to use for lekid f0's. First f0 in the array will be the first kid from the
    left.
    :param wavelength_spacing: Fraction of wavelength to space between stubs.
    :param readout_width: Width of readout cpw center line.
    :param readout_gap: Gap of readout cpw line.
    :param cell_name: Cell name for cascaded lekid cell.
    :param layers: Dictionary of the GDSII layers, must have the following keys: "back_etch", "niobium_microstrip",
    "aluminium_microstrip", "sin":, "niobium_ground", "wafer",
    :param pp_cap_lekids: Boolean to use parallel plate capacitor lekids. Default is IDC lekids.
    :param al_feedline: Boolean to use aluminium layer for CPW. If false, niobium is used.
    :param al_lekid: Boolean to use aluminium layer for LEKID. If false, niobium is used.
    :return: lekid_cascade_cell, length of cpw line.
    """

    lekid_cascade_cell = gdspy.Cell(name=cell_name)

    readout_connection_x = 0.0
    readout_connection_y = 0.0
    for count, f0 in enumerate(mm_f0_array):
        # Build lekid cell
        lekid = Lekid(
            target_f0=lekid_f0_array[count],
            signal_input_x=0.0,
            signal_input_y=0.0,
        )

        idc_layer = layers["idc"]
        if al_lekid:
            idc_layer = layers["aluminium"]

        cpw_centre_layer = layers["readout_centre"]  #
        if al_feedline:
            cpw_centre_layer = layers["aluminium"]

        lekid_cell = lekid.make_cell(
            e_beam_boundary_layer=layers["e_beam_boundary"],
            inductor_layer=layers["inductors"],
            idc_layer=idc_layer,
            readout_layer=cpw_centre_layer,
            ground_layer=layers["ground"],
            dielectric_layer=layers["dielectric"],
            readout_width=readout_width,
            readout_gap=readout_gap,
            cell_name=cell_name + " Lekid_" + str(count),
        )

        if pp_cap_lekids:
            lekid = PPLekid(
                target_f0=lekid_f0_array[count],
                signal_input_x=0.0,
                signal_input_y=0.0,
            )
            lekid_cell = lekid.make_cell(
                inductor_layer=layers["inductors"],
                device_plate_layer=layers["capacitor_plates"],
                readout_layer=layers["readout_centre"],
                ground_layer=layers["ground"],
                readout_width=readout_width,
                readout_gap=readout_gap,
                cell_name=cell_name + " PP_Cap_Lekid_" + str(count),
            )

        lekid_readout_connections = lekid.get_readout_connections(
            readout_width=readout_width
        )

        # Shift lekid so readout is along x axis.
        lekid_ref_cell = gdspy.CellReference(
            lekid_cell,
            origin=(
                readout_connection_x - lekid_readout_connections[0][0],
                readout_connection_y - lekid_readout_connections[0][1],
            ),
        )

        lekid_cascade_cell.add(lekid_ref_cell)

        # Calculate interconnection length required.
        wavelength = tools.convert_f0_to_physical_wavelength(f0)
        full_interconnection_length = wavelength * wavelength_spacing
        corrected_length = (
            full_interconnection_length - 140
        )  # Adjust length to account for length in lekid cell.

        if count != len(mm_f0_array) - 1:
            # Build interconnection

            tools.cpw_connect_parallel_lines(
                x1=readout_connection_x + 140,
                y1=readout_connection_y,
                x2=readout_connection_x + 140 + corrected_length,
                y2=readout_connection_y,
                centre_width=readout_width,
                ground_gap=readout_gap,
                bend_radius=5 * readout_width,
                centre_layer=cpw_centre_layer,
                ground_layer=layers["ground"],
                target_cell=lekid_cascade_cell,
                parallel_in_x=True,
            )

            readout_connection_x = (readout_connection_x + full_interconnection_length,)

        if count == len(mm_f0_array) - 1:
            readout_connection_x = (readout_connection_x + 140,)

        readout_connection_x = readout_connection_x[0]

    return lekid_cascade_cell, readout_connection_x


def get_nearest_neighbour(filter_spacing: float, df_split: float or np.array):
    """
    :param filter_spacing: Spacing of the filter-bank (0.25 or 0.375)
    :param df_split: Frequency Split required in MHz
    :return:
    """

    # Default values for quarter wave spacing:
    m = -1.8511
    c = 2.2460

    if filter_spacing == 0.375:
        m = -1.8511
        c = 2.0911

    return (df_split / (10**c)) ** (1 / m)


def get_df_split(nearest_neighbour: int or np.array, filter_spacing: float):
    """
    Returns the minimum df split in MHz for a neighbouring detector given which nearest neighbour it is.

    :param nearest_neighbour: Nearest neighbour at which df_split is required.
    :param filter_spacing: Spacing of the filter-bank (0.25 or 0.375)
    :return:
    """

    # Default values for quarter wave spacing:
    m = -1.8511
    c = 2.2460

    if filter_spacing == 0.375:
        m = -1.8511
        c = 2.0911

    return (nearest_neighbour**m) * (10**c)


def split_and_group_f0_array(
    f0_array: np.ndarray, number_of_groups: int, number_of_spectrometers: int
) -> np.ndarray:
    """
    Function to take an array of all the f0's on a readout line and split them into N groups. Each group is then split
    into sub groups where the number of sub groups is the number of filter-banks on a feedline. The corresponding
    sub-group of each main group is then combined together resulting in a subarray for each spectrometer.

    :param f0_array: Array to be mixed. Must have a size such that (f0_array size/N groups) is divisible by the number
    of spectrometers.
    :param number_of_groups: Number of groups (equivalent to the number of detectors between a detector and it's nearest
    frequency neighbour.
    :param number_of_spectrometers: Number of spectrometers on the feedline.
    :return:
    """

    group_size = f0_array.size / number_of_groups
    if group_size % number_of_spectrometers != 0:
        return print(
            f"Ensure the f0 array has a suitable size. Group size={group_size} and needs to be divisible by "
            f"{number_of_spectrometers} and {number_of_groups}"
        )

    f0_array = np.split(f0_array, number_of_groups)
    split_f0_array = []
    for group in f0_array:
        split_group_array = np.split(group, number_of_spectrometers)
        split_f0_array.append(split_group_array)

    split_f0_array = np.array(split_f0_array)

    spectrometer_arrays = np.empty(
        shape=(
            number_of_spectrometers,
            number_of_groups,
            int(group_size / number_of_spectrometers),
        )
    )
    for group_index, group in enumerate(split_f0_array):
        for sub_group_index, sub_group in enumerate(group):
            spectrometer_arrays[sub_group_index][group_index] = sub_group

    return spectrometer_arrays


def mix_spectrometer_f0_array(
    spectrometer_f0_array: np.ndarray, mixing_permutation: list
):
    """
    Function to take a spectrometer f0 array which is a 2D array containing N groups of sequential f0's where N is the
    number of groups the band is split into (detectors between nearest f0 neighbours) then mix according to the group
    permutation sequence given.

    :param spectrometer_f0_array: 2D Numpy array of all the f0's for one spectrometer.
    :param mixing_permutation: List of an index sequence corresponding to the arrangement of sub groups in the f0 array.
    :return:
    """

    # take every other element from each sub_array. Evens (0, 2, 4...) a_array, odds (1, 3, 5...) b_array:
    a_array = spectrometer_f0_array[mixing_permutation][:, ::2]
    b_array = spectrometer_f0_array[mixing_permutation][:, 1::2]

    # Logic to ensure b array has same shape as a array so can be dstacked
    if a_array.shape[1] > b_array.shape[1]:
        new_b_array = np.empty(a_array.shape)
        for index, sub_array in enumerate(b_array):
            new_b_array[index] = np.concatenate((sub_array, np.array([0.0])))
        b_array = new_b_array

    # dstack so sub arrays are now in pairs [[11, 12], [13, 14]], [[21, 22], [23, 24]]
    ab = np.dstack([a_array, b_array])

    # now the actual nearest frequency neighbour is on the opposite side of the mm feedline.
    bottom_f0_array = np.dstack(ab[:, :, 0]).flatten()
    top_f0_array = np.dstack(ab[:, :, 1]).flatten()

    mixed_array = np.dstack([bottom_f0_array, top_f0_array]).flatten()
    mixed_array = np.delete(mixed_array, np.where(bottom_f0_array == 0))

    return mixed_array


def find_best_f0_mixing_permutation(
    spectrometer_f0_array: np.ndarray, filter_spacing: float
) -> list:
    """
    Function to take a spectrometer f0 array which is a 2D array containing N groups of sequential f0's where N is the
    number of groups the band is split into (detectors between nearest f0 neighbours.

    :param spectrometer_f0_array: 2D Numpy array of all the f0's for one spectrometer.
    :param filter_spacing: Spacing of the filter-bank (0.25 or 0.375)
    :return:
    """

    plt.figure(figsize=(8, 6))
    plt.plot(
        spectrometer_f0_array.flatten() * 1e-9,
        linestyle="none",
        marker="o",
        markersize=4,
        label="Spectrometer F0 Array",
    )
    plt.xlabel("KID Index")
    plt.ylabel("F0 (GHz)")
    plt.legend()
    plt.show()

    number_of_sub_groups = spectrometer_f0_array.shape[0]
    sub_group_indices = np.linspace(
        0, number_of_sub_groups - 1, number_of_sub_groups, dtype=int
    )

    permutation_generator = itertools.permutations(sub_group_indices)

    # Score to track the quality of the df splits between neighbours for each permutation, returns the permutation
    # with the maximum score. Score is the total summed ratio of the mean actual df_split to the threshold.
    best_permutation_score = 0
    best_permutation = []
    for permutation_count, permutation in enumerate(permutation_generator):
        df_split_is_bad = (
            False  # Enable each permutation to be checked for valid df splits
        )
        permutation_score = 0  # Score for current permutation.
        permutation = list(permutation)

        mixed_f0_array = mix_spectrometer_f0_array(spectrometer_f0_array, permutation)

        bottom_f0_array = mixed_f0_array[::2]
        top_f0_array = mixed_f0_array[1::2]

        for nearest_neighbour in range(
            1, number_of_sub_groups + 1
        ):  # Cycle through nearest neighbours to check each satisfies threshold
            if df_split_is_bad:
                break

            for (
                start_index
            ) in (
                sub_group_indices
            ):  # Cycle through each unique starting point to see all slices
                # If statement to catch first index and use correct syntax
                if start_index == 0:
                    top_difference = np.diff(top_f0_array[::nearest_neighbour])
                    bottom_difference = np.diff(bottom_f0_array[::nearest_neighbour])
                else:
                    top_difference = np.diff(
                        top_f0_array[start_index::nearest_neighbour]
                    )
                    bottom_difference = np.diff(
                        bottom_f0_array[start_index::nearest_neighbour]
                    )

                # Required df_split for current nearest neighbour:
                threshold = (
                    get_df_split(
                        filter_spacing=filter_spacing,
                        nearest_neighbour=nearest_neighbour,
                    )
                    * 1e6
                )

                # If any splits are less than the df split threshold then break out of loop
                top_side_failures = np.where(np.abs(top_difference) < threshold)[0]
                bottom_side_failures = np.where(np.abs(bottom_difference) < threshold)[
                    0
                ]
                if top_side_failures.size != 0:
                    df_split_is_bad = True
                    # print(f"Df Split of {np.abs(top_difference[top_side_failures[0]]):.2E}Hz on top side for neighbour "
                    #       f"{nearest_neighbour} not below threshold of {threshold:.2E}Hz")
                    break
                if bottom_side_failures.size != 0:
                    df_split_is_bad = True
                    # print(f"Df Split of {np.abs(bottom_difference[bottom_side_failures[0]]):.2E}Hz on top side for neighbour "
                    #       f"{nearest_neighbour} not below threshold of {threshold:.2E}Hz")
                    break

                permutation_score += np.mean(top_difference) / threshold
                permutation_score += np.mean(bottom_difference) / threshold

        if permutation_score > best_permutation_score and not df_split_is_bad:
            print(f"suitable_permutation: {permutation}")
            best_permutation_score = permutation_score
            best_permutation = permutation

        # itertools permutation generator runs through each permutation sequentially, If theres no solution starting
        # with 0th index then there wont be a solution.
        if permutation[0] > 0:
            print("All permutations tried starting with 0th index")
            break

    if best_permutation_score == 0:
        return print("No suitable Permutation found")
    else:
        print(f"Suitable mixing array found: {best_permutation}")
        return best_permutation


def get_group_readout_bend_points(spectrometer_angle: float, input: bool, connection_coordinates: tuple):
    """
    Function to get the coordinates of the connecting bend on the input or output of a spectrometer group.

    :param spectrometer_angle: angle of connecting spectrometer in degrees
    :param input: Boolean whether this is the input or output
    :param connection_coordinates: Coordinates of the connection to the spectrometer readout.
    :return:
    """

    original_points = [
        (0.0, 0.0),
        (0.0, -500),
        (500, -500),
        (500, -1000),
        (0.0, -1000),
        (-300, 0.0),
    ]
    if not input:
        original_points = [
            (0.0, 0.0),
            (0.0, -500),
            (-500, -500),
            (-500, -1000),
            (0.0, -1000),
            (300, 0.0),
        ]

    # Rotate and translate points
    if spectrometer_angle > 90:
        spectrometer_angle = spectrometer_angle - 90
    elif spectrometer_angle < 90:
        spectrometer_angle = - 90 + spectrometer_angle
    final_points = []
    for point in original_points:
        rotated_point = tools.rotate_coordinates(point[0], point[1], angle=math.radians(spectrometer_angle))
        final_points.append(
            (rotated_point[0] + connection_coordinates[0], rotated_point[1] + connection_coordinates[1])
        )

    return final_points
