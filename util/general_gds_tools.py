import gdspy
import numpy as np
import math


def import_gds(file_path: str, cell_name=None):
    """
    Imports a GDS file and returns a cell with all the corresponding geometry polygons
    ----------
    :param file_path: Str, Path or name of file to be imported
    :param cell_name: Str, Name of the cell that will be returned as a Device.  If None, will automatically select the
    topmost cell.
    """

    imported_library = gdspy.GdsLibrary()
    imported_library.read_gds(file_path, units="convert")
    top_level_cells = imported_library.top_level()
    if len(top_level_cells) == 1:
        top_cell = top_level_cells[0]
    elif len(top_level_cells) > 1:
        raise ValueError(
            "import_gds() There are multiple top-level "
            "cells, you must specify `cell_name` to select of "
            "one of them"
        )
    top_cell.name = cell_name
    return top_cell


def bridge_builder(
    path_points: list,
    bridge_width: float,
    bridge_thickness: float,
    bridge_separation: float,
    bridge_layer: int,
    bridge_cell_id="",
) -> gdspy.Cell:
    """
    Function to build bridges along the path of a CPW path, connecting the ground planes.
    Returns the cell of the bridges.

    :param path_points: List of CPW path points.
    :param bridge_width: Width of the bridges, equivalent to the total CPW
    ground plane gap width.
    :param bridge_thickness: Thickness of the bridge, dimension parallel
    to path direction.
    :param bridge_separation: Distance between adjacent bridges.
    :param bridge_layer: GDSII layer for the bridges, should be the same
    as the ground planes.
    :param bridge_cell_id: String identifier to give bridge cell a unique name. Avoids multiples of the same cell being
    added to library.
    """

    bridge_cell = gdspy.Cell("Bridge Cell " + bridge_cell_id)
    bridges_cell = gdspy.Cell("Bridges Cell " + bridge_cell_id)

    bridge_cell.add(
        gdspy.Rectangle(
            point1=(-bridge_thickness / 2, bridge_width / 2 + 5),
            point2=(bridge_thickness / 2, -bridge_width / 2 - 5),
            layer=bridge_layer,
        )
    )

    for count, point in enumerate(path_points):
        # If to avoid going out of index at final point.
        if point != path_points[-1]:
            bridge_section_cell = gdspy.Cell(
                "Bridge Section " + bridge_cell_id + str(count)
            )

            end_point = path_points[count + 1]

            x1 = point[0]
            y1 = point[1]
            x2 = end_point[0]
            y2 = end_point[1]

            # Block to calculate required rotation angle of the bridge:
            vector = [x2 - x1, y2 - y1]
            unit_vector = vector / np.linalg.norm(vector)
            x_unit_vector = [1.0, 0.0]
            dot_product = np.dot(unit_vector, x_unit_vector)
            cross_product = np.cross(x_unit_vector, vector)
            angle = np.arccos(dot_product) / np.pi * 180
            # If cross product is less than zero, angle is counter clockwise, hence correction:
            if cross_product < 0:
                angle = 360 - angle

            # Block to place bridges centrally between both points at a fixed distance:
            distance = np.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
            number_of_bridges = math.floor(distance / bridge_separation)
            bridge_x_positions = np.linspace(
                start=1, stop=number_of_bridges, num=number_of_bridges
            )
            bridge_x_positions = (
                bridge_x_positions - np.sum(bridge_x_positions) / number_of_bridges
            )
            bridge_x_positions = distance / 2 + bridge_separation * bridge_x_positions

            # Loop to place bridges along the x axis at the required positions and spacings.
            for position in bridge_x_positions:
                bridge_cell_reference = gdspy.CellReference(
                    ref_cell=bridge_cell, origin=(position, 0.0), rotation=0.0
                )
                bridge_section_cell.add(bridge_cell_reference)

            # Create reference cell of bridge section and rotate to required angle.
            bridge_section_ref_cell = gdspy.CellReference(
                ref_cell=bridge_section_cell, origin=point, rotation=angle
            )
            bridges_cell.add(bridge_section_ref_cell)

    return bridges_cell


def calculate_bridge_total(path_points: list, bridge_separation: float):
    """
    Function to calculate how many bridges will be added along a given path with the bridge builder functions.

    :param path_points: List of CPW path points.
    :param bridge_separation: Distance between adjacent bridges.
    :return:
    """

    # Find total number of bridges that can fit within path:
    total_bridges = 0
    for count, point in enumerate(path_points):
        # If to avoid going out of index at final point.
        if point != path_points[-1]:
            end_point = path_points[count + 1]
            x1 = point[0]
            y1 = point[1]
            x2 = end_point[0]
            y2 = end_point[1]
            # Block to place bridges centrally between both points at a fixed distance:
            distance = np.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
            number_of_bridges = math.floor(distance / bridge_separation)
            total_bridges += number_of_bridges

    return total_bridges


def variable_bridge_builder(
    path_points: list,
    bridge_width: float,
    initial_bridge_thickness: float,
    final_bridge_thickness: float,
    bridge_separation: float,
    bridge_layer: int,
    bridge_cell_id="",
) -> gdspy.Cell:
    """
    Function to build bridges along the path of a CPW path, connecting the ground planes. The bridges vary in thickness
    between the initial and final thicknesses with a linear spacing defined by the number of bridges that will be fit in
    along the path length.
    Returns the cell of the bridges.

    :param path_points: List of CPW path points.
    :param bridge_width: Width of the bridges, equivalent to the total CPW
    ground plane gap width.
    :param initial_bridge_thickness: Thickness of first bridge.
    :param final_bridge_thickness: Thickness of final bridge.
    :param bridge_separation: Distance between adjacent bridges.
    :param bridge_layer: GDSII layer for the bridges, should be the same
    as the ground planes.
    :param bridge_cell_id: String identifier to give bridge cell a unique name. Avoids multiples of the same cell being
    added to library.
    """

    bridges_cell = gdspy.Cell("Transition Bridges Cell " + bridge_cell_id)

    total_bridges = calculate_bridge_total(
        path_points=path_points, bridge_separation=bridge_separation
    )
    bridge_thicknesses = np.linspace(
        initial_bridge_thickness, final_bridge_thickness, num=total_bridges
    )

    bridge_thickness_selector = 0
    for count, point in enumerate(path_points):
        # If to avoid going out of index at final point.
        if point != path_points[-1]:
            bridge_section_cell = gdspy.Cell(
                "Bridge Section " + bridge_cell_id + str(count)
            )

            end_point = path_points[count + 1]

            x1 = point[0]
            y1 = point[1]
            x2 = end_point[0]
            y2 = end_point[1]

            # Block to calculate required rotation angle of the bridge:
            vector = [x2 - x1, y2 - y1]
            unit_vector = vector / np.linalg.norm(vector)
            x_unit_vector = [1.0, 0.0]
            dot_product = np.dot(unit_vector, x_unit_vector)
            cross_product = np.cross(x_unit_vector, vector)
            angle = np.arccos(dot_product) / np.pi * 180
            # If cross product is less than zero, angle is counter clockwise, hence correction:
            if cross_product < 0:
                angle = 360 - angle

            # Block to place bridges centrally between both points at a fixed distance:
            distance = np.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
            number_of_bridges = math.floor(distance / bridge_separation)
            bridge_x_positions = np.linspace(
                start=1, stop=number_of_bridges, num=number_of_bridges
            )
            bridge_x_positions = (
                bridge_x_positions - np.sum(bridge_x_positions) / number_of_bridges
            )
            bridge_x_positions = distance / 2 + bridge_separation * bridge_x_positions

            # Loop to place bridges along the x axis at the required positions and spacings.
            for position in bridge_x_positions:
                # Select bridge thickness then increase selector so next bridge thickness is used.
                bridge_thickness = bridge_thicknesses[bridge_thickness_selector]
                bridge_thickness_selector += 1
                bridge = gdspy.Rectangle(
                    point1=(position - bridge_thickness / 2, bridge_width / 2),
                    point2=(position + bridge_thickness / 2, -bridge_width / 2),
                    layer=bridge_layer,
                )
                bridge_section_cell.add(bridge)

            # Create reference cell of bridge section and rotate to required angle.
            bridge_section_ref_cell = gdspy.CellReference(
                ref_cell=bridge_section_cell, origin=point, rotation=angle
            )
            bridges_cell.add(bridge_section_ref_cell)

    return bridges_cell


def build_cpw_with_bridges(
    path_points: list or np.ndarray,
    cpw_centre_width: float,
    cpw_gap: float,
    bend_radius: float,
    bridge_thickness: float,
    bridge_separation: float,
    cpw_centre_layer: int,
    ground_layer: int,
    cell_name="CPW with bridges",
) -> gdspy.Cell:
    """
    Function to build a cpw line with ground bridges given a set of path coordinates.

    :param path_points: Coordinates for the cpw to follow. e.g. [(x1, y1), (x2, y2), (x3, y3)]
    :param cpw_centre_width: Width of the cpw center line.
    :param cpw_gap: Gap width for the cpw.
    :param bend_radius: Radius to us for bends in cpw.
    :param bridge_thickness: Width of the cpw bridges.
    :param bridge_separation: Separation distance between bridges.
    :param cpw_centre_layer: GDSII layer for the cpw center line.
    :param ground_layer: GDSII layer for the ground plane and bridges.
    :param cell_name: Name for cpw with bridges cell.
    :return:
    """

    cpw_with_bridges_cell = gdspy.Cell(name=cell_name)

    centre_path = gdspy.FlexPath(
        points=path_points,
        width=cpw_centre_width,
        offset=0.0,
        corners="circular bend",
        bend_radius=bend_radius,
        max_points=1999,
        layer=cpw_centre_layer,
    )

    gap_path = gdspy.FlexPath(
        points=path_points,
        width=cpw_centre_width + 2 * cpw_gap,
        offset=0.0,
        corners="circular bend",
        bend_radius=bend_radius,
        max_points=1999,
        layer=ground_layer,
    )

    # Add bridges to north west and south west feedline tests.
    readout_bridges_cell = bridge_builder(
        path_points=path_points,
        bridge_width=cpw_centre_width + 2 * cpw_gap,
        bridge_thickness=bridge_thickness,
        bridge_separation=bridge_separation,
        bridge_layer=ground_layer,
        bridge_cell_id=cell_name,
    )
    # Remove bridges from readout_gap_path
    readout_gap_path_with_bridges = gdspy.boolean(
        operand1=gap_path,
        operand2=readout_bridges_cell,
        operation="not",
        layer=ground_layer,
    )

    cpw_with_bridges_cell.add(
        [
            centre_path,
            readout_gap_path_with_bridges,
        ]
    )

    return cpw_with_bridges_cell


def build_bridge_transition_cpw(
    path_points: list or np.ndarray,
    cpw_centre_width: float,
    cpw_gap: float,
    bend_radius: float,
    initial_bridge_thickness: float,
    final_bridge_thickness: float,
    bridge_separation: float,
    cpw_centre_layer: int,
    ground_layer: int,
    cell_name="CPW bridge transition",
) -> gdspy.Cell:
    """
    Function to build a cpw line with gradually changing bridge thicknesses. Bridge thickness is a linear spaced
    increment between initial and final bridge thicknesses for the number of bridges that can be fit in within the path
    length.

    :param path_points: Coordinates for the cpw to follow. e.g. [(x1, y1), (x2, y2), (x3, y3)]
    :param cpw_centre_width: Width of the cpw center line.
    :param cpw_gap: Gap width for the cpw.
    :param bend_radius: Radius to us for bends in cpw.
    :param initial_bridge_thickness: Thickness of first bridge.
    :param final_bridge_thickness: Thickness of final bridge
    :param bridge_separation: Separation distance between bridges.
    :param cpw_centre_layer: GDSII layer for the cpw center line.
    :param ground_layer: GDSII layer for the ground plane and bridges.
    :param cell_name: Name for cpw with bridges cell.
    :return:
    """

    cpw_with_bridges_cell = gdspy.Cell(name=cell_name)

    centre_path = gdspy.FlexPath(
        points=path_points,
        width=cpw_centre_width,
        offset=0.0,
        corners="circular bend",
        bend_radius=bend_radius,
        max_points=1999,
        layer=cpw_centre_layer,
    )

    gap_path = gdspy.FlexPath(
        points=path_points,
        width=cpw_centre_width + 2 * cpw_gap,
        offset=0.0,
        corners="circular bend",
        bend_radius=bend_radius,
        max_points=1999,
        layer=ground_layer,
    )

    # Add bridges to north west and south west feedline tests.
    readout_bridges_cell = variable_bridge_builder(
        path_points=path_points,
        bridge_width=cpw_centre_width + 2 * cpw_gap,
        initial_bridge_thickness=initial_bridge_thickness,
        final_bridge_thickness=final_bridge_thickness,
        bridge_separation=bridge_separation,
        bridge_layer=ground_layer,
        bridge_cell_id=cell_name,
    )
    # Remove bridges from readout_gap_path
    readout_gap_path_with_bridges = gdspy.boolean(
        operand1=gap_path,
        operand2=readout_bridges_cell,
        operation="not",
        layer=ground_layer,
    )

    cpw_with_bridges_cell.add(
        [
            centre_path,
            readout_gap_path_with_bridges,
        ]
    )

    return cpw_with_bridges_cell


def offset_polygons_on_layer(
    layer_to_reduce: int,
    output_layer: int,
    cell: gdspy.Cell,
    offset: float,
) -> gdspy.PolygonSet:
    """
    Function to copy all polygons on a given layer of a specified cell and offset each individual closed polygon by a
    specified amount.

    :param layer_to_reduce: GDSII layer corresponding to the polygons to copy.
    :param output_layer: GDSII layer that the offset polygons will be put on.
    :param cell: Cell containing the polygons to be offset.
    :param offset: Offset amount to shrink or grow polygons by. Positive grows, negative shrinks.
    :return:
    """

    offset_polygons = cell.get_polygons(by_spec=(layer_to_reduce, 0))
    offset_polygon_set = gdspy.PolygonSet(offset_polygons, layer=output_layer)
    # Merge all connecting polygons on layer into single closed polygons
    offset_polygon_set = gdspy.boolean(
        operand1=offset_polygon_set,
        operand2=offset_polygon_set,
        operation="or",
        layer=output_layer,
    )
    offset_polygon_set = gdspy.offset(
        offset_polygon_set, layer=output_layer, distance=offset
    )
    return offset_polygon_set


def copy_polygons_to_layer(
    layers_to_copy: list,
    output_layer: int,
    cell: gdspy.Cell,
) -> None:
    """
    Function to copy all polygons on a set of layers of a specified cell to another layer. Returns the polygon_set

    :param layers_to_copy: List of GDSII layers corresponding to the polygons to copy.
    :param output_layer: GDSII layer that the copied polygons will be put on.
    :param cell: Cell containing the polygons to be copied.
    :return:
    """

    for layer in layers_to_copy:
        layer_polygons = cell.get_polygons(by_spec=(layer, 0))
        layer_polygon_set = gdspy.PolygonSet(layer_polygons, layer=output_layer)
        cell.add(layer_polygon_set)

    return None


def extract_polygons_from_gds_file(filename: str, layer: int):
    """
    Extracts top level polygons from a gds file and returns the polygons. Use gdspy.PolygonSet to utilize the output.

    :param filename: Path to gds file.
    :param layer: Layer to extract.
    :return:
    """

    temp_library = gdspy.GdsLibrary(infile=filename)
    top_level_cell = temp_library.top_level()[0]
    pol_dict = top_level_cell.get_polygons(by_spec=True)
    polygons = pol_dict[(layer, 0)]

    return polygons


def get_top_cell_from_gds_file(filename: str, cell_name: str) -> gdspy.Cell:
    """
    Function to obtain the polygon dictionary (polygons for each layer) of the top level of a gds file.

    :param filename: Gds file
    :param cell_name: Name to give copied cell.
    :return:
    """

    temp_library = gdspy.GdsLibrary(infile=filename)
    top_level_cell = temp_library.top_level()[0]
    top_level_cell.name = cell_name

    return top_level_cell


def copy_top_level_cell_from_gds_file(filename: str, from_layers: list, to_layers: list, cell_name: str) -> gdspy.Cell:
    """
    Function to copy all the polygons from the top level of a gds file and add them to a cell, transferring the from
    layers to the given assigned layers.

    :param filename: file path to the desired gds file to copy.
    :param from_layers: List of GDSII layers (integers) in the gds file.
    :param to_layers: List of corresponding layers in the current library to which the from layers will be moved to.
    :param cell_name: Name for the output copied cell.
    :return:
    """

    new_cell = gdspy.Cell(cell_name)

    top_level_cell = get_top_cell_from_gds_file(filename=filename, cell_name=cell_name)
    polygon_dictionary = top_level_cell.get_polygons(by_spec=True)

    for count, layer in enumerate(from_layers):

        layer_polygons = polygon_dictionary[(layer, 0)]
        if len(layer_polygons) == 0:
            continue
        layer_polygon_set = gdspy.PolygonSet(layer_polygons, layer=to_layers[count])
        new_cell.add(layer_polygon_set)

    return new_cell


def translate_coordinates(position_1: list or tuple, position_2: list or tuple) -> list:
    """
    function to translate the coordinates of position_1 by the coordinates of position_2.
    i.e. (x1, y1) -> (x1 + x2, y1 + y2)

    :param position_1: coordinates of position 1
    :param position_2: coordinates of position 1
    :return:
    """

    new_position = [position_1[0] + position_2[0], position_1[1] + position_2[1]]

    return new_position
